# Copyright (C) 2015 TopCoder Inc., All Rights Reserved.

import bitarray
import copy
import binascii
import abc

# Additional pacakges
from pybloom import BloomFilter

# IPND Dependencies
from helper import Helper


class NBF(object):
    """
    ["2.6.4 Neighborhood Bloom Filter", pages 14 - 15]
    :param nbf_hashes: Hex string defining the hashes used to generate the bloomfilter (ignored)
    :param bits: A bytearray representing a bloomfilter. Would be used to initialize it.
    """
    # Max number of elements in the bloomfilter
    DEFAULT_CAPACITY = 20
    # Error chance of false positives
    DEFAULT_ERROR = 0.0001
    # For reference
    NBF_hashes = {"md5": 0x00, "murmur": 0x01, "sha256": 0x02}

    def __init__(self, nbf_hashes=None, bits=None):
        self.nbf_hashes = nbf_hashes
        # BloomFilter(capacity, error)
        # Must be able to store at least capacity elements while maintaining
        # no more than error chance of false positives
        self.bloomfilter = BloomFilter(self.DEFAULT_CAPACITY, self.DEFAULT_ERROR)
        if bits:
            self.bloomfilter.bitarray = bitarray.bitarray(endian="little")
            self.bloomfilter.bitarray.frombytes(str(bits))

    def add(self, eid):
        """
        Adds a node to the NBF.
        :param eid: EID of the node to add.
        """
        self.bloomfilter.add(eid)

    def contains(self, eid):
        """
        Checks if a node is contained inside the NBF
        :param eid: EID of the node to check.
        :return:
        """
        return eid in self.bloomfilter

    def compute(self):
        """
        Serializes the bloomfilter as a bytearray
        :return: Bytearray representation of the bloomfilter.
        """
        return self.bloomfilter.bitarray.tobytes()

    def __str__(self):
        """
        NBF string
        :return: NBF string
        """
        return "hashes: {0} hash: {1}".format(binascii.hexlify(self.nbf_hashes),
                                              binascii.hexlify(self.bloomfilter.bitarray))

    def __deepcopy__(self, memo):
        """
        NBF deepcopy.
        :param memo: Internal of the copy.deepcopy method.
        :return: NBF copy.
        """
        nbf_copy = NBF()
        nbf_copy.nbf_hashes = self.nbf_hashes
        nbf_copy.bloomfilter = self.bloomfilter.copy()

        return copy

    def __cmp__(self, other):
        """
        NBF comparision
        :param other: NBF object to compare.
        :return: True if equal, False otherwise.
        """
        equal = False
        if other and self.nbf_hashes and other.nbf_hashes and self.bloomfilter and self.bloomfilter:
            equal = self.nbf_hashes == other.nbf_hashes and \
                    self.bloomfilter.bitarray.tobytes() == other.bloomfilter.bitarray.tobytes()
        elif other and not self.nbf_hashes and not other.nbf_hashes and not self.bloomfilter and notself.bloomfilter:
            euqal = True

        return equal
        
class Beacon(object):
    __metaclass__ = abc.ABCMeta

    """
    Beacon base class.
    """
    IPND_version = 0x00

    def __init__(self):
        self.version = self.IPND_version
        self.flags = 0
        self.seq_number = 0
        self.eid = ""
        self.nbf = None
        self.period = 0
        
        self.send_time = None
        self.receive_time = None

    @abc.abstractmethod
    def __str__(self):
        """
        Beacon string
        :return: Beacon string representation
        """
        pass

    @abc.abstractmethod
    def __deepcopy__(self, memo):
        """
        Beacon deepcopy.
        :return Beacon copy.
        """
        pass


class BeaconV4(Beacon):
    """
    https://tools.ietf.org/html/draft-irtf-dtnrg-ipnd-02
    ["2.6. Beacon Message Format", pages 7 - 14]
    """
    IPND_version = 0x04
    Flags = {'source_eid': 0, 'service_block': 1, 'nbf': 2, 'beacon_period': 3}

    def __init__(self):
        Beacon.__init__(self)

        # Additional v4 parameters
        self.services = None
        self.nbf = None
        self.period = 0

        self.send_time = None
        self.receive_time = None

    def from_conf(self, conf, beacon_period=0, seq_number=1):
        """
        Generates a bundle from an IPNDConfig object
        :param conf: IPNDConfig object
        :param beacon_period: Period of the beacon
        :param seq_number: Sequence number of the beacon
        """

        if conf.announce_eid:
            self.flags = Helper.set_bit(self.flags, self.Flags['source_eid'])
            self.eid = conf.eid
        if conf.announce_beacon_period:
            self.flags = Helper.set_bit(self.flags, self.Flags['beacon_period'])
            self.period = beacon_period
        # Note: Service block and NBF bits are set when the services are added

        self.seq_number = seq_number

    def add_services(self, services):
        """
        Adds the services to be announced for this IPND instance
        :param services: IPNDService containing the services to be announced
        """
        services_list = services.get_services()
        if len(services_list) > 0:
            self.flags = Helper.set_bit(self.flags, self.Flags['service_block'])
            nbf_hashes = "nbf-hashes" in services_list and services_list["nbf-hashes"]
            nbf_bits = "nbf-bits" in services_list and services_list["nbf-bits"]
            if nbf_hashes and nbf_bits:
                # If NBF-Hashes and NBF-Bits services are present, create the NBF
                self.nbf = NBF(nbf_hashes=nbf_hashes.get_hash_id())
                # And set the NBF present bit
                self.flags = Helper.set_bit(self.flags, self.Flags['nbf'])
        self.services = services_list

    def update_nbf(self, neighbors):
        """
        Updates the beacon NBF
        :param neighbors: List of Neighbors to add to the NBF
        """
        if self.nbf:
            for nb in neighbors.itervalues():
                if nb.beacon and nb.beacon.eid:
                    self.nbf.add(str(nb.beacon.eid))
            # And update nbf-bits service too if it is present
            if "nbf-bits" in self.services:
                self.services["nbf-bits"].update_bit_array(self.nbf.compute())

    def __str__(self):
        """
        BeaconV4 string
        :return: BeaconV4 string representation.
        """
        string = "Version ({0})" \
                 " Flags ({1})" \
                 " Seq. number ({2})".format(self.version, bin(self.flags), self.seq_number)
        if Helper.test_bit(self.flags, self.Flags['source_eid']):
            string += " Source EID ({0})".format(self.eid)
        if Helper.test_bit(self.flags, self.Flags['service_block']):
            string += " Services"
            if self.services:
                for service_key, service_value in self.services.iteritems():
                    string += " ({0} : {1})".format(service_key, service_value)
            else:
                string += " None"
        if Helper.test_bit(self.flags, self.Flags['beacon_period']):
            string += " Beacon period ({0})".format(self.period)

        return string

    def __deepcopy__(self, memo):
        """
        Deepcopy method.
        :param memo: Internal of the copy.deepcopy method.
        :return: BeaconV4 copy.
        """
        beacon_copy = BeaconV4()
        beacon_copy.flags = self.flags
        beacon_copy.seq_number = self.seq_number
        beacon_copy.eid = self.eid
        beacon_copy.services = copy.deepcopy(self.services)
        beacon_copy.period = self.period
        beacon_copy.send_time = self.send_time
        beacon_copy.receive_time = self.receive_time

        return beacon_copy


class BeaconV2(Beacon):
    """
    https://tools.ietf.org/html/draft-irtf-dtnrg-ipnd-01
    ["2.6. Beacon Message Format", pages 7 - 9]
    """
    IPND_version = 0x02
    Flags = {'source_eid': 0, 'service_block': 1, 'nbf': 2}

    def __init__(self):
        Beacon.__init__(self)
        self.flags = 0
        self.seq_number = 0
        self.eid = ""

        self.send_time = None
        self.receive_time = None

    def __str__(self):
        """
        BeaconV2 string.
        :return: BeaconV2 string representation.
        """
        string = "Version ({0})" \
                 " Flags ({1})" \
                 " Seq. number ({2})".format(self.version, bin(self.flags), self.seq_number)
        if Helper.test_bit(self.flags, self.Flags['source_eid']):
            string += " Source EID ({0})".format(self.eid)
        if Helper.test_bit(self.flags, self.Flags['service_block']):
            string += " Services (ignored in IPND beacons version {0})".format(self.version)

        return string

    def __deepcopy__(self, memo):
        """
        BeaconV2 deepcopy
        :param memo: Internal of copy.deepcopy method
        :return:BeaconV2 copy
        """
        beacon_copy = BeaconV2()
        beacon_copy.flags = self.flags
        beacon_copy.seq_number = self.seq_number
        beacon_copy.eid = self.eid

        beacon_copy.send_time = self.send_time
        beacon_copy.receive_time = self.receive_time

        return beacon_copy