# Copyright (C) 2015 TopCoder Inc., All Rights Reserved.

import logging
import socket
import shlex
import sys
import threading

# Additional packages
import bs4

# IPND Dependencies
from IPNDService import *
from helper import Helper, GlobalVars


class IncorrectServiceFile(Exception):
    """
    Errors encountered while parsing service files
    """
    pass


class IPNDAdmin(object):

    """
    Loads and manages IPND services.
    Uses services definitions from files RESERVED_SERVICES_DEFINITIONS and
    PRIVATE_SERVICES_DEFINITIONS
    :param services: List of services to load.
    """
    RESERVED_SERVICES_DEFINITIONS = "conf/reserved_services.xml"
    PRIVATE_SERVICES_DEFINITIONS = "conf/private_services.xml"
    # IP and PORT where command server will listen
    COMMAND_PORT = 4500
    COMMAND_IP = "127.0.0.1"
    # Set to max UDP packet length
    MAX_COMMAND_LENGTH = 65507
    SERVER_TIMEOUT = 2.0

    def __init__(self, services=None):
        self.command_thread = None
        self.ipnd_service = IPNDService()
        self._setup()
        if services:
            for service_name, fields in services.items():
                try:
                    self.load_service(service_name.lower(), fields)
                except Exception as ex:
                    print ex
                # If nbf-hashes service is present, we have to add the
                # dynamically updated nbf-bits service too.
                if service_name.lower() == "nbf-hashes":
                    logging.debug("NBF-Hashes service configured, NBF-Bits service added.")
                    self.load_service("nbf-bits", {"bit_array": bytearray()})

        GlobalVars.IPND_ADMIN = self

    def _setup(self):
        """
        Loads RESERVED_SERVICES_DEFINITIONS and PRIVATE_SERVICES_DEFINITIONS files
        """
        reserved_services = self._load_file(self.RESERVED_SERVICES_DEFINITIONS)
        private_services = self._load_file(self.PRIVATE_SERVICES_DEFINITIONS)

        service_tags = reserved_services + private_services

        # Check that the loaded services and tags have the correct parameters defined
        self._check_integrity(service_tags)

        # Convert loaded tags to dictionaries for easy access
        self.services_definitions = self._construct_services_dictionary(service_tags)

    def _load_file(self, file_to_load):
        """
        Loads services definitions from a xml file.
        :param file_to_load: File to load.
        """
        # Load and parse file
        with open(file_to_load, 'r') as constructed_services_file:
            constructed_services_content = constructed_services_file.read()
        bs = bs4.BeautifulSoup(constructed_services_content, "lxml")

        # Load service definitions
        services = bs.findAll("service")

        return services

    def _check_integrity(self, services):
        """
        Checks the integrity of the loaded services definitions.
        :param services: BeautifulSoup array of services to check
        """
        for service in services:
            if type(service) == bs4.Tag:
                if not service.has_attr("id") or not service.has_attr("name"):
                    raise IncorrectServiceFile("Service {0} does not have the required format".format(service))
            for param in service:
                if type(param) == bs4.Tag:
                    if not param.has_attr("name") or not param.has_attr("type"):
                        raise IncorrectServiceFile("Param {0} does not have the required format".format(param))

    def _construct_services_dictionary(self, services):
        """
        Converts a list of services into a dictionary using the name service as key for easy access.
        :param services: BeautifulSoup array of services to convert.
        """
        dictionary = dict()
        for service in services:
            if type(service) == bs4.Tag:
                # We convert all ids to decimal
                service_id = service["id"].lower()
                if service_id.startswith("0x"):
                    service_id = int(service_id, 16)
                service_name = service["name"].lower()
                dictionary[service_name] = {"id": service_id}
                dictionary[service_name]["params"] = dict()
                params = dict()
                for param in service.children:
                    if type(param) == bs4.Tag:
                        params[param["name"].lower()] = dict()
                        params[param["name"].lower()]["type"] = param["type"].lower()
                        if param.has_attr("pack"):
                            params[param["name"].lower()]["pack"] = param["pack"].lower()
                dictionary[service_name]["params"] = params
        return dictionary

    @staticmethod
    def construct_service(service_definition, fields):
        """
        Constructs a Service from its definition and fields.
        :param service_definition: Dictionary describing the definition of the service.
        :param fields: Dictionary describing the configuration of the service.
        :return A constructed Service.
        """
        service_id = int(service_definition["id"])
        service = None
        if not Helper.test_bit(service_id, 7):
            if Helper.test_bit(service_id, 6):
                if service_id == 126:
                    # Special case NBF-Hashes
                    service = NBFHashesService(service_definition, fields)
                elif service_id == 127:
                    # Special case NBF-Bits
                    service = NBFBitsService(service_definition, fields)                    
                else:
                    # Constructed service
                    service = ConstructedService(service_definition, fields)
            else:
                # Primitive Service
                service = PrimitiveService(service_definition, fields)
        if Helper.test_bit(service_id, 7):
            # Private Service
            service = PrivateService(service_definition, fields)

        return service

    def load_service(self, service_name, fields):
        """
        Loads a service into IPND. 
        :param service_name: Name of the service .
        :param fields: Dictionary containing the fields of the service.
                       The keys should correspond with the name of the tags defined in th service definition.
        """
        if service_name.lower() in self.services_definitions:
            service_definition = self.services_definitions[service_name.lower()]
            service = self.construct_service(service_definition, fields)
            self.ipnd_service.add_service(service_name.lower(), service)
        else:
            raise IncorrectServiceFile("Structure of service {0} not defined.".format(service_name))

    def unload_service(self, service_name):
        """
        Unloads a service from IPND.
        :param service_name: Service to unload.
        """
        self.ipnd_service.remove_service(service_name)

    def get_service(self, service_name):
        """
        Gets a loaded service.
        :param service_name: Service to retrieve.
        """
        return self.ipnd_service.get_service(service_name.lower())

    def get_all_services(self):
        """
        Gets the IPNDService instance associated to the IPNDAdmin
        """
        return self.ipnd_service

    def send_response(self, address, msg):
        """
        :param address: Address to where to send the message.
        :param msg: Message to send.
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(msg, address)
        logging.info(msg)

    def command_server_thread(self, ip, port):
        """
        Executes an UDP server that listens for commands at (COMMAND_IP, COMMAND_PORT)
        """
        logging.info("Started command server at {0}:{1}".format(ip, port))

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((self.COMMAND_IP, self.COMMAND_PORT))
        sock.settimeout(self.SERVER_TIMEOUT)

        while GlobalVars.ALIVE:
            try:
                command, address = sock.recvfrom(self.MAX_COMMAND_LENGTH, )
            except socket.timeout:
                continue
            except socket.error as ex:
                logging.error("Error at command server: {0}".format(ex))
            logging.info("Received command: {0}".format(command.strip()))

            # Tokenize command
            try:
                ct = dict(token.split('=') for token in shlex.split(command))
            except Exception:
                self.send_response(address, "Invalid command received: {0}".format(command))
                continue

            # Interpret command
            if "add" in ct and "name" in ct:
                if ct["add"] == "service":
                    # Extract name
                    service_name = ct["name"]
                    # Prepare params
                    del ct["add"]
                    del ct["name"]
                    try:
                        self.load_service(service_name, ct)
                        self.send_response(address, "Service {0} loaded".format(service_name))
                    except Exception as ex:
                        self.send_response(address, "Service {0} not loaded: {1}".format(service_name, ex))
                else:
                    self.send_response(address, "Action not recognized: {0}".format(ct["add"]))
            elif "remove" in ct and "name" in ct:
                if ct["remove"] == "service":
                    # Extract name
                    service_name = ct["name"]
                    try:
                        self.unload_service(service_name)
                        self.send_response(address, "Service {0} removed".format(service_name))
                    except Exception as ex:
                        self.send_response(address, "Service {0} no removed: {1}".format(service_name, ex))
                else:
                    self.send_response(address, "Action not recognized: {0}".format(ct["add"]))
            else:
                self.send_response(address, "Command {0} not recognized".format(command))

    def start_command_server(self, ip=COMMAND_IP, port=COMMAND_PORT):
        """
        Starts a listening server thread.
        :param ip: IP where to bind.
        :param port: Port to where to bind.
        """
        self.command_thread = threading.Thread(target=self.command_server_thread, args=(ip, port))
        self.command_thread.start()

def main():
    """
    Establishes an UPD connection with the IPND service to add or remove 
    services while IPND is running.
    """
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(("0.0.0.0", 0))
        sock.sendto(" ".join(sys.argv[1:]), (IPNDAdmin.COMMAND_IP, IPNDAdmin.COMMAND_PORT))
        response = sock.recv(IPNDAdmin.MAX_COMMAND_LENGTH)
        print "Response: {0}".format(response)
    except Exception as ex:
        print "Error occurred: {0}".format(ex)

if __name__ == '__main__':
    main()
