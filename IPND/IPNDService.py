# Copyright (C) 2015 TopCoder Inc., All Rights Reserved.

import abc
import threading
import copy
import binascii
import logging

# IPND Dependencies
from helper import Helper


class InvalidServiceDefinition(Exception):
    """
    Errors encountered while creating a service.
    """
    pass


class IPNDService(object):
    """
    Stores a set of IPND services
    """

    def __init__(self):
        self.lock = threading.Lock()
        self._list = dict()

    def add_service(self, service_name, service):
        """
        Adds a service.
        :param service_name: Name of the service to add.
        :param service: Definition of the service to add.
        """
        self.lock.acquire()
        self._list[service_name] = service
        self.lock.release()
        logging.info("Added service {0}".format(service_name))

    def get_service(self, service_name):
        """
        Gets a service.
        :param service_name: Get the service_name.
        :return: The Service service_name if it is found. None otherwise.
        """
        self.lock.acquire()
        if service_name in self._list:
            to_return = copy.deepcopy(self._list[service_name.lower()])
        else:
            to_return = None
        self.lock.release()

        return to_return

    def update_service(self, service_name, service):
        """
        Updates a service.
        :param service_name: Service to update.
        :param service: New service definition.
        """
        self.lock.acquire()
        self._list[service_name] = service
        self.lock.release()
        logging.info("Updated service {0}".format(service_name))

    def remove_service(self, service_name):
        """
        Removes a service.
        :param service_name: Service to remove.
        """
        self.lock.acquire()
        if service_name in self._list:
            del self._list[service_name]
        self.lock.release()
        logging.info("Removed service {0}".format(service_name))

    def get_services(self):
        """
        Returns a copy of all the contained services
        :return: Copy of all the contained services.
        """
        self.lock.acquire()
        to_return = copy.deepcopy(self._list)
        self.lock.release()

        return to_return


class Service(object):
    """
    Base class for constructing services

    :param definition: Definition of the service.
    :param fields: Fields defining the service.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, definition=None, fields=None):
        self.tag = int(definition["id"])
        if not definition:
            self.definition = dict()
        else:
            self.definition = definition

        if not fields:
            self.fields = dict()
        else:
            self.fields = fields

    def __hash__(self):
        """
        :return Service hash,
        """
        return hash(self.tag)

    def __str__(self):
        """
        :return Service string.
        """
        string = ""
        for key, value in self.fields.iteritems():
            string_value = value
            if type(value) == bytearray:
                string_value = "0x" + binascii.hexlify(value).upper()
            string += "{0}={1} ".format(key, string_value)
        return string

    def __cmp__(self, other):
        """
        Service comparison
        :param other: Service to compare.
        """
        return self.tag == other.tag and \
               cmp(self.fields, other.fields) and \
               cmp(self.definition, other.definition)

    def __deepcopy__(self, memo):
        """
        Deepcopy method.
        :param memo: Internal of the copy.deepcopy method.
        :return: Service copy.
        """
        copy = object.__new__(self.__class__)
        copy.tag = self.tag
        copy.definition = self.definition.copy()
        copy.fields = self.fields.copy()

        return copy


class ConstructedService(Service):
    """
    Constructs a constructed service.

    :param definition: Definition of the service.
    :param fields: Fields defining the service.
    """

    def __init__(self, definition=None, fields=None):
        Service.__init__(self, definition, fields)
        # Check that the tag is correct
        if Helper.test_bit(self.tag, 7) or not Helper.test_bit(self.tag, 6):
            raise InvalidServiceDefinition("Service tag does not correspond to a constructed service")


class NBFHashesService(ConstructedService):
    """
    Constructs a NBF-Hashes service.

    :param definition: Definition of the service.
    :param fields: Fields defining the service.
    """

    def __init__(self, definition=None, fields=None):
        ConstructedService.__init__(self, definition, fields)
        # Check that the tag is correct
        if self.tag != 126:
            raise InvalidServiceDefinition("Service tag does not correspond to a NBF-Hashes service")

    def get_hash_id(self):
        """
        Returns the hashes ids of the service.
        """
        return self.fields["hash_id"]


class NBFBitsService(ConstructedService):
    """
    Constructs a NBF-Bits service.

    :param definition: Definition of the service.
    :param fields: Fields defining the service.
    """

    def __init__(self, definition=None, fields=None):
        ConstructedService.__init__(self, definition, fields)
        # Check that the tag is correct
        if self.tag != 127:
            raise InvalidServiceDefinition("Service tag does not correspond to a NBF-Bits service")

    def update_bit_array(self, bit_array):
        """
        Updates the NBF
        :param bit_array: New bit array
        """
        self.fields["bit_array"] = bit_array

    def get_bit_array(self):
        """
        Gets the NBF bitarray
        :return: NBF bitarray
        """
        return self.fields["bit_array"]

    def __str__(self):
        """
        NBF-Bits string.
        """
        string = ""
        for key, value in self.fields.iteritems():
            string_value = "0x" + binascii.hexlify(value).upper()
            string += "{0}={1} ".format(key, string_value)
        return string


class PrivateService(Service):
    """
    Constructs a Private service.

    :param definition: Definition of the service.
    :param fields: Fields defining the service.
    """

    def __init__(self, definition=None, fields=None):
        Service.__init__(self, definition, fields)
        # Check that the tag is correct
        if not Helper.test_bit(self.tag, 7):
            raise InvalidServiceDefinition("Service tag does not correspond to a private service")


class PrimitiveService(Service):
    """
    Constructs a Primitive service.

    :param definition: Definition of the service.
    :param fields: Fields defining the service.
    """

    def __init__(self, definition=None, fields=None):
        Service.__init__(self, definition, fields)
        # Check that the tag is correct
        if Helper.test_bit(self.tag, 7) or Helper.test_bit(self.tag, 6):
            raise InvalidServiceDefinition("Service tag does not correspond to a primitive service")
