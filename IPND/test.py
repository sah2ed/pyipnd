# Copyright (C) 2015 TopCoder Inc., All Rights Reserved.

import unittest
import random
import string
import copy

from node import *
from beacon import *
from IPND import *
from IPNDAdmin import *
from helper import *
from docker import Client


class TestNBF(unittest.TestCase):
    """
    Unit tests for the Neighborhood Bloom Filter
    """

    def test_nbf(self):
        # Test EIDs
        node1 = "dtn://TestNode1"
        node2 = "dtn://TestNode2"

        # Create new BNF
        bnf = NBF(["md5"])

        # Add elements
        bnf.add(node1)
        bnf.add(node2)

        # Test elements
        self.assertTrue(bnf.contains(node1))
        self.assertTrue(bnf.contains(node2))

        # Serialize NBF
        bs = bnf.compute()

        # Create new BloomFilter from serialized
        b2 = NBF(["md5"], bs)
        # Test elements again
        self.assertTrue(b2.contains(node1))
        self.assertTrue(b2.contains(node2))


class TestServices(unittest.TestCase):
    """
    Unit tests for Service creation and serialization
    """

    def test_services(self):
        services = dict()
        ipnd_admin = IPNDAdmin()
        tlv = TLV(["conf/data_types.xml", "conf/private_services.xml"])

        # Testing creation of Services using definitions from XML files and a dictionary

        # Creation of reserved constructed types
        cla_tcp_v4_params = {"ip": "10.0.0.1", "port": 1111}
        ipnd_admin.load_service("CLA-TCP-v4", cla_tcp_v4_params)
        cla_tcp_v4_service = ipnd_admin.get_service("cla-tcp-v4")
        self.assertIsInstance(cla_tcp_v4_service, ConstructedService)
        services["CLA-TCP-v4"] = cla_tcp_v4_service

        # Creation of special services NBF-Hashes and NBF-Bits
        nbf_hashes_params = {"hash_id": "0x0204"}
        ipnd_admin.load_service("NBF-Hashes", nbf_hashes_params)
        nb_hashes_service = ipnd_admin.get_service("NBF-Hashes")
        self.assertIsInstance(nb_hashes_service, NBFHashesService)
        services["NBF-Hashes"] = nb_hashes_service

        nbf_bits_params = {"bit_array": "0x00000000010100020004"}
        ipnd_admin.load_service("NBF-Bits", nbf_bits_params)
        nbf_bits_service = ipnd_admin.get_service("NBF-Bits")
        self.assertIsInstance(nbf_bits_service, NBFBitsService)
        services["NBF-Bits"] = nbf_bits_service

        # Creation of private constructed types
        foorouter_params = {"seed": "0xB4A1", "baseweight": "0x123F", "roothash": "0xDEADBEEF04"}
        ipnd_admin.load_service("foorouter", foorouter_params)
        foorouter_service = ipnd_admin.get_service("foorouter")
        self.assertIsInstance(foorouter_service, PrivateService)
        services["foorouter"] = foorouter_service

        # Testing serialization of services

        # CLA-TCP-v4 serialization
        cla_tcp_c4_signature_array = ['40', '08', '04', '0A', '00', '00', '01', '03', '04', '57']
        cla_tcp_c4_signature = bytearray.fromhex(''.join(cla_tcp_c4_signature_array))
        serialized_cla_tcp_v4 = tlv.encode_service(services["CLA-TCP-v4"])
        self.assertEqual(cla_tcp_c4_signature, serialized_cla_tcp_v4)

        # NBF-Hashes serialization
        nbf_hashes_signature_array = ['7E', '04', '09', '02', '02', '04']
        nbf_hashes_signature = bytearray.fromhex(''.join(nbf_hashes_signature_array))
        serialized_nbf_hashes = tlv.encode_service(services["NBF-Hashes"])
        self.assertEqual(nbf_hashes_signature, serialized_nbf_hashes)

        # NBF-Bits serialization
        nbf_bits_signature_array = ['7F', '0C', '09', '0A', '00', '00', '00', '00', '01', '01', '00', '02', '00', '04']
        nbf_bits_signature = bytearray.fromhex(''.join(nbf_bits_signature_array))
        serialized_nbf_bits = tlv.encode_service(services["NBF-Bits"])
        self.assertEqual(nbf_bits_signature, serialized_nbf_bits)

        # foorouter serialization
        # Appendix B.  Fictional Private Service Example [page 25]
        # The order appears changed but this signature is also correct, from Appendix B:
        # Note that the ordering of the service's composition does not exactly match the
        # definition; this should not be an issue for a receiving node with
        # knowledge of the FooRouter service.

        foorouter_signature_array = ['80', '11',  # Foorouter ID and total length
                                     '09', '05', 'DE', 'AD', 'BE', 'EF', '04',  # Roothash
                                     '82', '03', '03', '12', '3F',  # WeightVal
                                     '81', '03', '03', 'B4', 'A1']  # SeedVal
        foorouter_signature = bytearray.fromhex(''.join(foorouter_signature_array))
        serialized_foorouter = tlv.encode_service(services["foorouter"])
        self.assertEqual(foorouter_signature, serialized_foorouter)


class TestBeaconSerialization(unittest.TestCase):
    """
    Tests the correct serialization and deserialization of the Beacon
    """
    NUM_RANDOM_BEACONS = 1000
    MAX_EID_LEN = 128

    def setUp(self):
        """
        Adds equal methods for comparing the different parts of the Beacon
        """
        super(TestBeaconSerialization, self).setUp()

    def random_ip(self):
        """
        Generates a random IP
        :return: Random IP
        """
        return socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))

    def random_string(self, n):
        """
        Generates a random string
        :param n: Length of the string
        :return: Random string
        """
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(n))

    def random_beacon(self):
        """
        Generates a beacon randomly.
        :return: Randomly generated Beacon
        """
        ipnd_admin = IPNDAdmin()

        beac = BeaconV4()
        # Set random set flags
        beac.flags = random.getrandbits(4)
        # Set random seq. number
        beac.seq_number = random.randint(0, 1000)
        if Helper.test_bit(beac.flags, 0):  # EID present
            # Add random EID
            beac.eid = self.random_string(random.randint(1, self.MAX_EID_LEN))
        if Helper.test_bit(beac.flags, 1):  # Service Block present
            beac.services = dict()
            # Add CLA-TCP-v4 randomly
            if bool(random.getrandbits(1)):
                cla_tcp_v4_params = {"ip": str(self.random_ip()), "port": random.randint(0, 65535)}
                ipnd_admin.load_service("CLA-TCP-v4", cla_tcp_v4_params)
            # Add foorouter randomly
            if bool(random.getrandbits(1)):
                foorouter_params = {"seed": "0xB4A1", "baseweight": "0x123F", "roothash": "0xDEADBEEF04"}
                ipnd_admin.load_service("foorouter", foorouter_params)
            # Add random NBF
            if Helper.test_bit(beac.flags, 2):  # NBF present
                nbf_hashes = []
                for i in range(0, len(NBF.NBF_hashes)):
                    nbf_hashes.append(NBF.NBF_hashes.keys()[i])
                nbf = NBF(nbf_hashes=nbf_hashes)
                # Add a random number of neighbors to the bloomfilter
                for i in range(0, random.randint(0, NBF.DEFAULT_CAPACITY)):
                    nbf.add(self.random_ip())
                beac.nbf = nbf

                ipnd_admin.load_service("NBF-Hashes", {"hash_id": str(nbf.nbf_hashes)})
                ipnd_admin.load_service("NBF-Bits", {"bit_array": str(nbf.compute())})

            beac.services = ipnd_admin.get_all_services().get_services()
        else:
            # If there is no service block there can't be an NBF
            beac.flags = Helper.unset_bit(beac.flags, 2)
        # If Beacon period present bit set, add a random beacon period.
        if Helper.test_bit(beac.flags, 3):  # Beacon period present
            beac.period = random.randint(0, 100)

        return beac

    def test_random_beacons(self):
        """
        Generates self.NUM_RANDOM_BEACONS beacons randomly, serializes and deserializes them
        and checks that the deserialized beacon equals the randomly generated beacon.
        :return:
        """
        helper = Helper()
        for num in range(0, self.NUM_RANDOM_BEACONS):
            beac = self.random_beacon()
            sb = helper.encode_beacon(beac)
            ub = helper.decode_beacon(sb, len(sb))
            self.assertFalse(Helper.diff_beacons(beac, ub))

    def test_diff_beacons(self):
        """
        Tests comparison of Beacons with helper.diff_beacons
        """
        beac_a = self.random_beacon()
        beac_b = self.random_beacon()

        # Make sure we have two different beacons
        beac_a.eid = "eid1"
        beac_b.eid = "eid2"
        self.assertTrue(Helper.diff_beacons(beac_a, beac_b))

        # Create two identical beacons
        beac_c = copy.deepcopy(beac_a)
        beac_c.services = copy.deepcopy(beac_a.services)
        self.assertFalse(Helper.diff_beacons(beac_a, beac_c))


class SystemTests():
    """
    Performs several system tests using docker containers as remote nodes
    """
    # Max time that each system test will run before considering that the test has failed.
    MAX_SYSTEM_TEST_TIME = 20
    SOCKET_DAEMON_PATH = "unix://var/run/docker.sock"
    LOG_FILE = "/tmp/IPND_system_tests.log"
    DOCKER_IPND_IMAGE_NAME = "dtn_test/ipnd"
    DOCKER_DTN2_IMAGE_NAME = "dtn_test/dtn2"
    DOCKER_IBR_IMAGE_NAME = "dtn_test/ibr"

    def __init__(self):
        # Connect to docker
        self.docker_cli = self.connect_to_docker()
        # Reset logging
        logging.shutdown()
        reload(logging)
        # Reconfigure logging
        logging.basicConfig(format='%(levelname)s %(asctime)s: %(message)s',
                            level=logging.DEBUG,
                            filename=self.LOG_FILE,
                            filemode='w')

    def connect_to_docker(self):
        """
        Connects to a running socket daemon
        :return: Docker client connection handler
        """
        cli = Client(base_url=self.SOCKET_DAEMON_PATH)
        return cli

    def follow(self, thefile):
        """
        From http://www.dabeaz.com/generators/follow.py
        Follow a file like tail -f.
        :param thefile: File to follow
        :return: each line of the file as it is being updated
        """
        thefile.seek(0, 2)
        while True:
            line = thefile.readline()
            if not line:
                time.sleep(0.1)
                continue
            yield line

    def start_local_node(self, conf):
        """
        Starts a local node using the supplied configuration
        :param conf: IPND configuration
        :return: Started node
        """
        # Load IPNDAdmin instance to manage services
        ipnd_admin = IPNDAdmin(conf.advertised_services)

        local_node = Node(conf)
        local_node.start()

        return local_node

    def stop_local_node(self):
        """
        Stops a local node
        :node node: Node to stop
        """
        GlobalVars.ALIVE = False
        time.sleep(5)

    def reset_local_node_state(self, node):
        """
        Resets the state of a node
        :param node: Node to reset
        """
        GlobalVars.ALIVE = True

    def start_general_docker_container(self, image_name, command, environment_vars=None):
        """
        Start a docker container using the supplied parameters.
        :param image_name: Name of the docker image to start.
        :param command: The start command for this container.
        :return: Docker id
        """
        if not environment_vars:
            environment_vars = dict()
        nb = self.docker_cli.create_container(image_name, command=command, environment=environment_vars)
        nb_id = nb.get('Id')
        self.docker_cli.start(nb_id)

        return nb_id

    def start_dtn2_docker_container(self, conf_file):
        """
        Start a DTN2 docker container using the supplied configuration file.
        :param conf_file: Configuration file passed to DTN2
        :return: Docker id
        """
        command = "dtnd -c {0}".format(conf_file)
        return self.start_general_docker_container(self.DOCKER_DTN2_IMAGE_NAME, command)

    def start_ibr_docker_container(self, conf_file):
        """
        Start a IBR docker container using the supplied configuration file.
        :param conf_file: Configuration file passed to IBR
        :return: Docker id
        """
        command = "dtnd -c {0}".format(conf_file)
        return self.start_general_docker_container(self.DOCKER_IBR_IMAGE_NAME, command)

    def start_ipnd_docker_container(self, conf_file):
        """
        Start an IPND docker container using the supplied configuration file
        :param conf_file: Configuration file passed to the IPND PoC
        :return: Docker id
        """
        command = "python /opt/IPND/IPND.py -c /opt/IPND/conf/{0}".format(conf_file)
        return self.start_general_docker_container(self.DOCKER_IPND_IMAGE_NAME, command)

    def stop_docker_container(self, docker_id):
        """
        Stop a docker contaianer
        :param docker_id: Docker id to stop
        """
        self.docker_cli.kill(docker_id)
        self.docker_cli.remove_container(docker_id)

    def discover_unknown_neighbor_multicast(self):
        """
        Tests the discovery of unknown neighbors through muticast.
        """
        print "Starting unknown neighbor"
        unknown_nb = self.start_ipnd_docker_container("IPND-node-no-enumerated-nbs.conf")

        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node-no-enumerated-nbs.conf")
        local_node = self.start_local_node(conf)

        print "Waiting for discovery of unknown neighbor "
        start_time = time.time()
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            match = re.match("INFO (.*) Sender (.*) is an unknown neighbor", line)
            if match:
                print "Detected unknown neighbor: {0} at {1}".format(match.group(2), match.group(1))
                print "Test successful"                
                break
            elif time.time() > start_time + self.MAX_SYSTEM_TEST_TIME:
                print "Test failed"
                break

        print "Stopping nodes"
        self.stop_docker_container(unknown_nb)
        self.stop_local_node()
        self.reset_local_node_state(local_node)

    def discover_unknown_neighbor_broadcast(self):
        """
        Tests the discovery of unknown neighbors through broadcast.
        """
        print "Starting unknown neighbor"
        unknown_nb = self.start_ipnd_docker_container("IPND-node-only-broadcast.conf")

        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node-only-broadcast.conf")
        local_node = self.start_local_node(conf)

        print "Waiting for discovery of unknown neighbor "
        start_time = time.time()
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            match = re.match("INFO (.*) Sender (.*) is an unknown neighbor", line)
            if match:
                print "Detected unknown neighbor: {0} at {1}".format(match.group(2), match.group(1))
                print "Test successful"                
                break
            elif time.time() > start_time + self.MAX_SYSTEM_TEST_TIME:
                print "Test failed"
                break

        print "Stopping nodes"
        self.stop_docker_container(unknown_nb)
        self.stop_local_node()
        self.reset_local_node_state(local_node)

    def discover_known_neighbors(self):
        """
        Tests the discovery of known neighbors
        """
        print "Starting known neighbor"
        known_nb = self.start_ipnd_docker_container("IPND-node-no-enum-no-multicast.conf")

        # Get known NB IP
        node_info = self.docker_cli.inspect_container(known_nb)
        known_nb_ip = node_info['NetworkSettings']['IPAddress']
        print "Known neighbor IP: {0}".format(known_nb_ip)

        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node-no-enum-no-multicast.conf")
        conf.additional_destinations.append(Address(known_nb_ip))
        local_node = self.start_local_node(conf)

        print "Waiting for discovery of known neighbor"
        start_time = time.time()
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            match = re.match("INFO (.*) Sender (.*) is an unknown neighbor", line)
            if match:
                print "Detected known neighbor: {0} at {1}, log:".format(match.group(2), match.group(1))
                num_lines = 3
                for line in loglines:
                    print line,
                    num_lines -= 1
                    if num_lines == 0:
                        break
                print "Test successful"                        
                break
            elif time.time() > start_time + self.MAX_SYSTEM_TEST_TIME:
                print "Test failed"
                break

        print "Stopping nodes"
        self.stop_docker_container(known_nb)
        self.stop_local_node()
        self.reset_local_node_state(local_node)

    def neighbor_expiration(self):
        """
        Tests neighbor expiration
        """
        print "Starting three neighbors"
        nb1 = self.start_ipnd_docker_container("IPND-node.conf")
        nb2 = self.start_ipnd_docker_container("IPND-node.conf")
        nb3 = self.start_ipnd_docker_container("IPND-node.conf")

        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node.conf")
        local_node = self.start_local_node(conf)

        print "Waiting for discovery of the three neighbors."
        start_time = time.time()
        neighbors_discovered = 0
        neighbors_expired = 0
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            discovery_match = re.match("INFO (.*) Sender (.*) is an unknown neighbor", line)
            expiring_match = re.match("INFO (.*) neighbor (.*) has disappeared", line)

            if discovery_match:
                neighbors_discovered += 1
                print "Detected known neighbor: {0} at {1}".format(discovery_match.group(2), discovery_match.group(1))
                if neighbors_discovered == 3:
                    print "Stopping the three neighbors"
                    self.stop_docker_container(nb1)
                    self.stop_docker_container(nb2)
                    self.stop_docker_container(nb3)
            elif expiring_match:
                neighbors_expired += 1
                print "Neighbor {0} expired".format(expiring_match.group(2))
                if neighbors_expired == 3:
                    print "Test successful"                    
                    break
            elif time.time() > start_time + self.MAX_SYSTEM_TEST_TIME:
                print "Test failed"
                break

        print "Stopping local node"
        self.stop_local_node()
        self.reset_local_node_state(local_node)

    def data_substitution_unicast(self):
        """
        Tests if data sent to a unicast address can substitute a beacon
        """
        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node.conf")
        local_node = self.start_local_node(conf)

        # Get local address from one of the node receiving socket
        local_address = local_node.receiving_sockets[0].getsockname()[0]

        print "Sending data from another node to local address {0}".format(local_address)
        data_sender = self.docker_cli.create_container(self.DOCKER_IPND_IMAGE_NAME,
                                                       command='python /opt/IPND/test_send_data.py {0} 4551'.format(
                                                           local_address))
        data_sender_id = data_sender.get('Id')
        self.docker_cli.start(data_sender_id)

        print "Waiting for reception of data"
        start_time = time.time()
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            match = re.match("INFO (.*) Received data (.*) is not a beacon", line)
            if match:
                print "Data received, log:"
                num_lines = 2
                for line in loglines:
                    print line,
                    num_lines -= 1
                    if num_lines == 0:
                        break
                print "Test successful"                        
                break
            elif time.time() > start_time + self.MAX_SYSTEM_TEST_TIME:
                print "Test failed"
                break

        print "Stopping nodes"
        self.stop_docker_container(data_sender_id)
        self.stop_local_node()
        self.reset_local_node_state(local_node)

    def data_substitution_multicast(self):
        """
        Tests if data sent to a multicast address can substitute a beacon
        """
        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node.conf")
        local_node = self.start_local_node(conf)

        print "Sending data from another node to multicast address {0}".format(conf.multicast_discovery_address)
        data_sender = self.docker_cli.create_container(self.DOCKER_IPND_IMAGE_NAME,
                                                       command='python /opt/IPND/test_send_data.py {0} 4551'
                                                       .format(conf.multicast_discovery_address))
        data_sender_id = data_sender.get('Id')
        self.docker_cli.start(data_sender_id)

        print "Waiting for reception of data"
        start_time = time.time()
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            match = re.match("INFO (.*) Received data (.*) is not a beacon", line)
            if match:
                print "Data received, log:"
                num_lines = 2
                for line in loglines:
                    print line,
                    num_lines -= 1
                    if num_lines == 0:
                        break
                print "Test successful"                        
                break
            elif time.time() > start_time + self.MAX_SYSTEM_TEST_TIME:
                print "Test failed"
                break

        print "Stopping nodes"
        self.stop_docker_container(data_sender_id)
        self.stop_local_node()
        self.reset_local_node_state(local_node)

    def data_substitution_broadcast(self):
        """
        Tests if data sent to a broadcast address can substitute a beacon
        """
        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node-only-broadcast.conf")
        local_node = self.start_local_node(conf)

        print "Sending data from another node to broadcast address 255.255.255.255"
        data_sender = self.docker_cli.create_container(self.DOCKER_IPND_IMAGE_NAME,
                                                       command='python /opt/IPND/test_send_data.py '
                                                               '255.255.255.255 4551 -b')
        data_sender_id = data_sender.get('Id')
        self.docker_cli.start(data_sender_id)

        print "Waiting for reception of data"
        start_time = time.time()
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            match = re.match("INFO (.*) Received data (.*) is not a beacon", line)
            if match:
                print "Data received, log:"
                num_lines = 2
                for line in loglines:
                    print line,
                    num_lines -= 1
                    if num_lines == 0:
                        break
                print "Test successful"                        
                break
            elif time.time() > start_time + self.MAX_SYSTEM_TEST_TIME:
                print "Test failed"
                break

        print "Stopping nodes"
        self.stop_docker_container(data_sender_id)
        self.stop_local_node()
        self.reset_local_node_state(local_node)

    def negative_test(self):
        """
        Tests if no neighbors are detected if there aren't neighbors around
        """
        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node.conf")
        local_node = self.start_local_node(conf)

        start_time = time.time()
        print "Waiting {0} seconds for neighbor discovering.".format(self.MAX_SYSTEM_TEST_TIME)
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            unknown_match = re.match("INFO (.*) Sender (.*) is an unknown neighbor", line)
            known_match = re.match("INFO (.*) Sender (.*) is an known neighbor", line)
            if unknown_match or known_match:
                print "Test failed: Neighbor discovered"
                break
            elif time.time() > start_time + self.MAX_SYSTEM_TEST_TIME:
                print "Test successful"                
                break

        print "Stopping nodes"
        self.stop_local_node()
        self.reset_local_node_state(local_node)

    def private_services_reception(self):
        """
        Tests if IPND correctly sends and receives private constructed services.
        """
        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node-private-constructed-services.conf")
        local_node = self.start_local_node(conf)

        print "Starting IPND node."
        ipnd_container_id = self.start_ipnd_docker_container("IPND-node-private-constructed-services.conf")

        start_time = time.time()
        print "Waiting for FooRouter service discovering."
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            match = re.match("DEBUG (.*) Beacon contents: (.*) \(foorouter : (.*) \)", line)
            if match:
                print "Beacon with FooRouter {0} service received.".format(match.group(3))
                print "Test successful"
                break
            elif time.time() > (start_time + self.MAX_SYSTEM_TEST_TIME):
                print "Test failed: FooRouter service not discovered."
                break

        self.stop_local_node()
        self.reset_local_node_state(local_node)
        self.stop_docker_container(ipnd_container_id)

    def dtn2_private_services_reception(self):
        """
        Tests if IPND correctly detects the FooRouter service announced by DTN2
        """
        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node-private-constructed-services.conf")
        local_node = self.start_local_node(conf)

        print "Starting DTN2 node."
        dtn2_container_id = self.start_dtn2_docker_container("/etc/dtn2/dtn_multicast.conf")

        start_time = time.time()
        print "Waiting for FooRouter service discovering."
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            match = re.match("DEBUG (.*) Beacon contents: (.*) \(foorouter : (.*) \)", line)
            if match:
                print "Beacon with FooRouter {0} service received.".format(match.group(3))
                print "Test successful"                
                break
            elif time.time() > (start_time + self.MAX_SYSTEM_TEST_TIME):
                print "Test failed: FooRouter service not discovered."
                break

        self.stop_local_node()
        self.reset_local_node_state(local_node)
        self.stop_docker_container(dtn2_container_id)

    def ibr_reception(self):
        """
        Tests if IPND correctly detects the FooRouter service announced by DTN2
        """
        print "Starting local IPND"
        conf = IPNDConfig("conf/IPND-node-private-constructed-services.conf")
        local_node = self.start_local_node(conf)

        print "Starting IBR node."
        ibr_container_id = self.start_ibr_docker_container("/etc/ibrdtn/ibrdtnd_multicast.conf")

        start_time = time.time()
        print "Waiting for Beacon reception."
        loglines = self.follow(open(self.LOG_FILE, "r"))
        for line in loglines:
            match = re.match("DEBUG (.*) Beacon contents: (.*)", line)
            if match:
                print "Beacon: {0} received.".format(match.group(2))
                print "Test successful"                
                break
            elif time.time() > (start_time + self.MAX_SYSTEM_TEST_TIME):
                print "Test failed: Beacon not detected"
                break

        self.stop_local_node()
        self.reset_local_node_state(local_node)
        self.stop_docker_container(ibr_container_id)

    def run(self):
        """
        Runs all system tests
        """

        print "---- System test 1: Discover known neighbors via unicast."
        self.discover_known_neighbors()

        print "---- System test 2: Discover unknown neighbors via multicast."
        self.discover_unknown_neighbor_multicast()

        print "---- System test 3: Discover unknown neighbors via broadcast."
        self.discover_unknown_neighbor_broadcast()

        print "---- System test 4: Neighbor expiration."
        self.neighbor_expiration()

        print "---- System test 5: Data substitute beacon (from unicast address)"
        self.data_substitution_unicast()

        print "---- System test 6: Data substitute beacon (from multicast address)"
        self.data_substitution_multicast()

        print "---- System test 7: Data substitute beacon (from broadcast address)"
        self.data_substitution_broadcast()

        print "---- System test 8: Negative test (no multicast, unicast or broadcast neighbors found)"
        self.negative_test()

        print "---- System test 9: Sending and reception of constructed private services"
        self.private_services_reception()

        print "---- System test 10: Reception of constructed private services from DTN2 (DTN2 interoperability)"
        self.dtn2_private_services_reception()

        print "---- System test 11: Reception and detection of beacons IPND V2 (IBR interoperability)"
        self.ibr_reception()

        print "---- System tests ended. System test log: {0}".format(self.LOG_FILE)


if __name__ == '__main__':
    print "Executing unit tests"
    unittest.TextTestRunner(verbosity=2).run(unittest.TestLoader().loadTestsFromTestCase(TestNBF))
    unittest.TextTestRunner(verbosity=2).run(unittest.TestLoader().loadTestsFromTestCase(TestServices))
    unittest.TextTestRunner(verbosity=2).run(unittest.TestLoader().loadTestsFromTestCase(TestBeaconSerialization))

    print "Executing system tests"
    system_tests = SystemTests()
    system_tests.run()

    print "All tests run."