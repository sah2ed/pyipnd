# Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
# Sends data to ip (argv[1]) and port (argv[2]). Used by the system tests.

import socket, sys

# Create socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
# Allow sending of broadcast apckets
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
# Send DATA
sock.sendto("DATA", (sys.argv[1], int(sys.argv[2])))