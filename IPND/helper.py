# Copyright (C) 2015 TopCoder Inc., All Rights Reserved.

import os
import sys
import logging

# IPND dependencies
sys.path.append(os.path.join(os.path.dirname(__file__), "lib"))
from lib.ipnd_sd_tlv import *


class GlobalVars():
    def __init__(self):
        """
        Class used to store the global variables of the IPND program
        """
        pass

    # Global variable used to terminate the threads from the signal handler
    ALIVE = True
    IPND_ADMIN = None


class Helper():
    """
    Class that provides helper methods to the IPND program.
    """

    def __init__(self):
        self.tlv = TLV(["conf/data_types.xml", "conf/private_services.xml"])

    # Bit testing

    @staticmethod
    def test_bit(byteval, idx):
        """
        Tests if bit idx in byteval is set.
        :param byteval: Bits to test
        :param idx: Position of the bit to test.
        :return: True if set, False otherwise.
        """
        return byteval & (1 << idx)

    @staticmethod
    def set_bit(byteval, idx):
        """
        Sets idx bit of byteval.
        :param byteval: Bits to set
        :param idx: Position of the bit to set.
        :return: byteval with bit idx set.
        """
        return byteval | (1 << idx)

    @staticmethod
    def unset_bit(byteval, idx):
        """
        Unsets idx bit of byteval.
        :param byteval: Bits to unset
        :param idx: Position of the bit to unset.
        :return: byteval with bit idx unset.
        """
        return byteval & ~(1 << idx)

    # Beacon encoding
    def encode_service_block(self, services):
        """
         Serializes a ServiceBlock

        :param services: services to serialize
        :return: Services serialized as a bytearray.
        """
        construct = bytearray()
        num_services = 0
        for service in services.itervalues():
            try:
                construct += self.tlv.encode_service(service)
                num_services += 1
            except Exception as ex:
                logging.error("Error encoding service {0}: {1}".format(service, ex))

        serialized_sb = bytearray()
        self.tlv.append_sdnv(serialized_sb, num_services)
        serialized_sb += construct

        return serialized_sb

    def encode_beacon(self, _beacon):
        """
        Serializes a beacon.
        :param _beacon: Beacon to serialize
        :return: Beacon serialized as a bytearray.
        """
        serialized = bytearray()

        # ["2.6. Beacon Message Format", pages 7 - 14]
        serialized.append(_beacon.version)
        serialized.append(_beacon.flags)
        seq_number_packed = struct.pack("!h", _beacon.seq_number)
        for byte in seq_number_packed:
            serialized.append(byte)
        if Helper.test_bit(_beacon.flags, _beacon.Flags['source_eid']):  # Add EID
            self.tlv.append_sdnv(serialized, len(_beacon.eid))
            for byte in _beacon.eid:
                serialized.append(byte)
        if Helper.test_bit(_beacon.flags, _beacon.Flags['service_block']):  # Add Service Block
            service_block_serialized = self.encode_service_block(_beacon.services)
            for byte in service_block_serialized:
                serialized.append(byte)
        if Helper.test_bit(_beacon.flags, _beacon.Flags['beacon_period']):  # Add Beacon Period
            self.tlv.append_sdnv(serialized, _beacon.period)

        return serialized

    # Beacon decoding
    def decode_service_block(self, serialized):
        """
        Decodes a service block.
        :param serialized: Serialized service block.
        :return: A dictionary containing the reconstructed Services.
        """
        services = dict()
        off = 0

        # Get number of services
        num_services, num_services_len = sdnv_decode(serialized[off:])
        off += num_services_len

        for num_service in range(0, num_services):
            # Get service ID
            service_id = serialized[off]
            off += 1

            # Find service in service definitions.
            found_service = False
            for service_name, service_definition in GlobalVars.IPND_ADMIN.services_definitions.iteritems():
                if int(service_definition["id"]) == service_id:
                    found_service = True
                    break
            if not found_service:
                logging.error('Unknown service with id {0}'.format(service_id))

            # Decode service fields
            service_fields, service_len = self.tlv.decode_service(serialized[off:], service_definition)

            # Reconstruct and store service
            service = IPNDAdmin.construct_service(service_definition, service_fields)
            services[service_name] = service

            # Next service
            off += service_len

        return off, services

    def decode_beacon_v2(self, serialized):
        """
        Decodes a beacon version 2.
        :param serialized: Serialized beacon
        :return: Reconstructed BeaconV2.
        """
        off = 0
        beac = beacon.BeaconV2()

        if beac.version != beacon.BeaconV2.IPND_version:
            return None

        off += 1
        beac.flags = serialized[off]
        off += 1
        beac.seq_number = struct.unpack("!h", serialized[off:off + 2])[0]
        off += 2
        if Helper.test_bit(beac.flags, 0):  # EID present
            eid_len, length = sdnv_decode(serialized[off:])
            off += length
            beac.eid = str(serialized[off:off + eid_len])
            off += eid_len

        return beac

    def decode_beacon_v4(self, serialized, serialized_len):
        """
        Decodes a beacon version 4.
        :param serialized: Serialized beacon
        :return: Reconstructed BeaconV4.
        """        
        off = 0
        beac = beacon.BeaconV4()

        if beac.version != beacon.BeaconV4.IPND_version:
            return None

        off += 1
        beac.flags = serialized[off]
        off += 1
        beac.seq_number = struct.unpack("!h", serialized[off:off + 2])[0]
        off += 2
        if Helper.test_bit(beac.flags, 0):  # EID present
            eid_len, length = sdnv_decode(serialized[off:])
            off += length
            beac.eid = str(serialized[off:off + eid_len])
            off += eid_len
        if Helper.test_bit(beac.flags, 1):  # Service Block present
            service_block_len, beac.services = self.decode_service_block(serialized[off:serialized_len])
            off += service_block_len
            # Reconstruct NBF if nbf-hashes and nbf-bits services are present
            if "nbf-hashes" in beac.services and "nbf-bits" in beac.services:
                # If NBF-Hashes and NBF-Bits services are present, create the NBF
                beac.nbf = beacon.NBF(nbf_hashes=beac.services["nbf-hashes"].get_hash_id(),
                                      bits=beac.services["nbf-bits"].get_bit_array())

        if Helper.test_bit(beac.flags, 3):  # Beacon period present
            beac.period, length = sdnv_decode(serialized[off:])
            off += length

        return beac

    def decode_beacon(self, serialized, serialized_len):
        """
        Deserializes a Beacon
        :param serialized: Serialized Beacon
        :param serialized_len: Length of the serialized Beacon
        :return: The Beacon deserialized
        """
        version = serialized[0]

        if int(version) == 0x02:
            return self.decode_beacon_v2(serialized)
        elif int(version) == 0x04:
            return self.decode_beacon_v4(serialized, serialized_len)
        else:
            return None

    @staticmethod
    def diff_services(s1, s2):
        """
        Compares two services dicts

        :param s1: First dict of services to compare.
        :param s2: Second dict of services to compare.
        :return: True if different, False otherwise.
        """
        equal = False
        if s1 and s2:
            equal = len(s1) == len(s2)
            for service, data in s1.iteritems():
                equal = equal and service in s1
                equal = equal and data == s2[service]
        elif not s1 and not s2:
            equal = True

        return not equal

    @staticmethod
    def diff_beacons(b1, b2):
        """
        Compares two Beacons
        :param b1: First Beacon to compare
        :param b2: Second beacon to compare
        :return: True if different, False otherwise.
        """
        equal = False
        if b2 and b2:
            equal = b1.version == b2.version and \
                    b1.flags == b2.flags and \
                    b1.eid == b2.eid and not \
                    Helper.diff_services(b1.services, b2.services) and \
                    b1.nbf == b2.nbf and \
                    b1.period == b2.period
        elif not b1 and not b2:
            equal = True

        return not equal

# Import IPND dependencies at the end to avoid circular imports (http://effbot.org/zone/import-confusion.htm)
import beacon
from IPNDAdmin import IPNDAdmin
