# Copyright (C) 2015 TopCoder Inc., All Rights Reserved.

import signal
import ConfigParser
import argparse
import logging

# IPND Dependencies
from helper import sys, GlobalVars
from node import Node, Address
from IPNDAdmin import IPNDAdmin


class IPNDConfig(object):

    """
    Loads IPND configuration from a config file.

    :param config_file: Configuration file path, default 'conf/IPND-node.conf'
    """
    DEFAULT_CONFIG_FILE = 'conf/IPND-node.conf'
    DEFAULT_BROADCAST_DISCOVERY = True
    DEFAULT_MULTICAST_TTL = 32
    DEFAULT_ANNOUNCEMENT_PERIOD = 1

    def __init__(self, config_file=DEFAULT_CONFIG_FILE):
        self.config_file = config_file
        self.additional_destinations = []
        self._load()

    def _load(self):
        """
        Loads options from file
        """
        config = ConfigParser.ConfigParser()
        config.readfp(open(self.config_file))

        logging.info("Loading configuration from %s", self.config_file)

        # Load eid
        try:
            self.announce_eid = config.getboolean('IPND', 'announce_eid')
        except ConfigParser.NoOptionError:
            logging.debug("announce_eid option not set. Not announcing eid.")
            self.announce_eid = False
        finally:
            try:
                self.eid = config.get('IPND', 'eid')
            except ConfigParser.NoOptionError:
                logging.error("announce_eid is set but eid option is not present. Not announcing eid.")
                self.announce_eid = False

        # Load port
        try:
            self.port = config.getint('IPND', 'port')
        except ConfigParser.NoOptionError:
            logging.error("port not set. Required option not set, aborting execution.")
            sys.exit(0)

        # Load beacon period announcement
        try:
            self.announce_beacon_period = config.getboolean('IPND', 'announce_beacon_period')
        except ConfigParser.NoOptionError:
            logging.debug("announce_beacon_period option not set. Beacon period will not be announced")
            self.announce_beacon_period = False

        # Load multicast address
        try:
            self.multicast_discovery_address = Address(config.get('IPND', 'multicast_discovery_address'))
        except ConfigParser.NoOptionError:
            logging.debug("multicast_discovery_address not set.")
            self.multicast_discovery_address = None

        # Load multicast ttl
        try:
            self.multicast_discovery_ttl = config.getint('IPND', 'multicast_discovery_ttl')
        except ConfigParser.NoOptionError:
            logging.debug("multicast_discovery_ttl not set. Setting default %d.", self.DEFAULT_MULTICAST_TTL)
            self.multicast_discovery_ttl = self.DEFAULT_MULTICAST_TTL

        # Load broadcast discovery
        try:
            self.broadcast_discovery = config.getboolean('IPND', 'broadcast_discovery')
            if self.broadcast_discovery:
                self.additional_destinations.append(Address("255.255.255.255"))
        except ConfigParser.NoOptionError:
            logging.debug("broadcast_discovery not set. Setting default %d.", self.DEFAULT_BROADCAST_DISCOVERY)
            self.broadcast_discovery = self.DEFAULT_BROADCAST_DISCOVERY

        # Load enumerated neighbors
        try:
            self.enumerated_nbs = map(Address, (map(str.strip, config.get('IPND', 'enumerated_neighbors').split(','))))
        except ConfigParser.NoOptionError:
            logging.debug("enumerated_nbs option not set.")
            self.enumerated_nbs = []

        try:
            # Load services
            self.advertised_services = dict.fromkeys(map(str.strip,
                                                     config.get('IPND', 'advertised_services')
                                                     .split(',')))
            # Add all services as a dictionary with all the options
            for service in self.advertised_services:
                try:
                    self.advertised_services[service] = dict(config.items(service))
                except ConfigParser.NoSectionError:
                    logging.debug("Service {0} configured to be announced but section is missing. "
                                  "The service won't be announced".format(service))
        except ConfigParser.NoOptionError:
            logging.debug("advertised_services option not set. Not announcing any service.")

        # Load beacon intervals periods
        try:
            self.beacon_interval_broadcast = config.getint('IPND', 'beacon_interval_broadcast')
        except ValueError:
            logging.debug("beacon_interval_broadcast option not set. Setting default %d.",
                          self.DEFAULT_ANNOUNCEMENT_PERIOD)
            self.beacon_interval_broadcast = self.DEFAULT_ANNOUNCEMENT_PERIOD
        try:
            self.beacon_interval_multicast = config.getint('IPND', 'beacon_interval_multicast')
        except ValueError:
            logging.debug("beacon_interval_multicast option not set. Setting default %d.",
                          self.DEFAULT_ANNOUNCEMENT_PERIOD)
            self.beacon_interval_multicast = self.DEFAULT_ANNOUNCEMENT_PERIOD
        try:
            self.beacon_interval_unicast = config.getint('IPND', 'beacon_interval_unicast')
        except ValueError:
            logging.debug("beacon_interval_unicast option not set. Setting default %d.",
                          self.DEFAULT_ANNOUNCEMENT_PERIOD)
            self.beacon_interval_unicast = self.DEFAULT_ANNOUNCEMENT_PERIOD


# Notifies threads to stop
def signal_handler(sig, frame):
    """
    Handler executed when a signal is catch
    :param sig: Signal number
    :param frame: Current stack frame
    """
    GlobalVars.ALIVE = False


def main():
    """
    Main IPND method
    """

    # Parse command line options
    parser = argparse.ArgumentParser("IPND protocol PoC")
    parser.add_argument('--config_file', '-c',
                        help='IPND configuration file. Default {0}.'.format(IPNDConfig.DEFAULT_CONFIG_FILE),
                        action='store',
                        default=IPNDConfig.DEFAULT_CONFIG_FILE,
                        type=str)
    parser.add_argument('--log_file', '-l',
                        help='log to file.',
                        action='store',
                        default=None,
                        type=str)
    parser.add_argument('--debug', '-d',
                        help='print debug information.',
                        action='store_true')
    options = parser.parse_args()

    if options.debug:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO

    # Register signal handler
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    # Prepare logging
    logging.basicConfig(format='%(levelname)s %(asctime)s: %(message)s',
                        level=log_level,
                        filename=options.log_file,
                        filemode='w')

    # Explicitly log INFO and above to console
    if options.log_file:
        console = logging.StreamHandler()
        console.setLevel(log_level)
        logging.getLogger('').addHandler(console)

    # Load configuration form args or default
    conf = IPNDConfig(options.config_file)

    # Load IPNDAdmin instance to manage services
    ipnd_admin = IPNDAdmin(conf.advertised_services)

    # Start IPNDAdmin command server thread. This thread opens a UDP socket and allows
    # the configuration of the announced servers while IPND is running using the client
    # IPNDAdmin.py
    ipnd_admin.start_command_server()

    # Start IPND node
    node = Node(conf)
    node.start()

    # Wait for end (Control-C, Control-D) signal
    signal.pause()

    # Exit process
    sys.exit(0)

# Main function
if __name__ == "__main__":
    main()
