# Copyright (C) 2015 TopCoder Inc., All Rights Reserved.

import socket
import struct

from bs4 import BeautifulSoup, Tag
from sdnv import sdnv_encode, sdnv_decode


class IncorrectTypeLength(Exception):
    """
    Type errors.
    """
    pass


class InvalidTypeDefinition(Exception):
    """
    Type errors.
    """
    pass


class MissingTypeDefinition(Exception):
    """
    Type errors.
    """
    pass


class InvalidTypeLength(Exception):
    """
    Type errors.
    """
    pass


class UnknownParam(Exception):
    """
    Parameters errors.
    """
    pass


class MissingParam(Exception):
    """
    Parameters errors.
    """
    pass


class TLV(object):
    """
    Encodes and decodes services using IPND-SD-TLV format
    :param types_files_path: List of XML files with type definition tags.
    """

    def __init__(self, types_files_path=None):
        self.types = dict()

        if types_files_path:
            if isinstance(types_files_path, list):
                for types_file_path in types_files_path:
                    self.load_types(types_file_path)
        else:
            self.load_types(types_files_path)

    def _load_file(self, file_to_load):
        """
        Loads services definitions from a XML file.
        :param file_to_load: Path to the XML file to load.
        """
        # Load and parse file
        # os.path.dirname(__file__) + "/"
        with open(file_to_load, 'r') as constructed_services_file:
            constructed_services_content = constructed_services_file.read()
        bs = BeautifulSoup(constructed_services_content, "lxml")

        # Load custom and primitive types
        types = bs.findAll("type")

        return types

    def _check_integrity(self, types):
        """
        Checks integrity of the loaded type tags.
        :param types: BeautifulSoup array of type tags
        """
        for _type in types:
            if type(_type) == Tag:
                if not _type.has_attr("name") or not _type.has_attr("type"):
                    raise InvalidTypeDefinition("Incorrect type {0}".format(_type))

    def _construct_types_dict(self, types):
        """
        Converts a list of types directly extracted from a XML file
        into a dictionary using the type name as key.
        :param types: BeautifulSoup array of type tags
        :return Dictionary created from the array of tags
        """
        dictionary = dict()
        for _type in types:
            if type(_type) == Tag:
                # Convert type id to integer
                type_id_int = _type["id"]
                if type_id_int.startswith("0x") and type_id_int[2:] > 10:
                    type_id_int = int(type_id_int, 16)
                elif type_id_int.startswith("0x"):
                    type_id_int = type_id_int[2:]

                dictionary[_type["name"].lower()] = {"id": type_id_int, "type": _type["type"]}
                if _type.has_attr("type_length"):
                    dictionary[_type["name"]]["type_length"] = _type["type_length"]
                if _type.has_attr("length"):
                    dictionary[_type["name"].lower()]["length"] = _type["length"]

        return dictionary

    def load_types(self, types_file_path):
        """
        Load types definitions
        :param types_file_path: Path of a XML file that containts the type tags.
        """
        types = self._load_file(types_file_path)
        self._check_integrity(types)
        new_types = self._construct_types_dict(types)
        self.types.update(new_types)

    def append_bytes(self, serialized, value, length=None):
        """
        Appends an array of bytes to serialized.
        :param serialized: Bytearray where to append the value
        :param value: Value to append.
        :param length: Expected length of the serialized value.

        :return Length of the serialized value
        """
        if value.startswith("0x"):
            # Value is hexadecimal:
            bytearray_value = bytearray.fromhex(''.join(value[2:]))
        else:
            bytearray_value = bytearray(value)

        if length and len(bytearray_value) != length:
            raise IncorrectTypeLength("Expected length: {0}, real length: {1}"
                                      .format(length, len(bytearray_value)))

        for byte in bytearray_value:
            serialized.append(byte)

        return len(bytearray_value)

    def append_sdnv(self, serialized, value):
        """
        Appends a SDNV encoded value to the serialized bytearray
        :param serialized: Bytearray to where to append the SDNV encoded value.
        :param value: Value to append.
        :return: Length of the encoded SDNV value
        """
        sdnv_value = sdnv_encode(value)
        for byte in sdnv_value:
            serialized.append(byte)
        return len(sdnv_value)

    def serialize_param(self, serialized, param_definition, param_value):
        """
        Serializes a service param. A param can be of two types:
            * primitive: in that case we just use encode the param as required.
            * wrapped: We recursively look for the param definition until we find the 
                primitive type.
        :param serialized: Bytearray where to append the serialized service param.
        :param param_definition: Definition of the param to append.
        :param param_value: Dictionary containing the necessary values to construct the serialized service.
        """
        param = self.types[param_definition["type"]]
        # First we append the id of the param
        serialized.append(int(param["id"]))
        if param["type"] == "primitive":
            # Process optional pack option
            if "pack" in param_definition:
                if param_definition["pack"] == "ipv4":
                    param_value = socket.inet_aton(param_value)
                if param_definition["pack"] == "ipv6":
                    ip6 = socket.inet_pton(socket.AF_INET6, param_value)
                    a, b = struct.unpack(">QQ", ip6)
                    param_value = str((a << 64) | b)
                elif param_definition["pack"] == "int":
                    param_value = struct.pack("!H", int(param_value))

            # If the parameter is primitive we can directly encode it
            # Then depending of the type_length:
            if param["type_length"] == "fixed":
                self.append_bytes(serialized, param_value, int(param["length"]))
            elif param["type_length"] == "variable":
                self.append_sdnv(serialized, param_value)
            elif param["type_length"] == "explicit":
                construct = bytearray()
                self.append_bytes(construct, param_value)
                self.append_sdnv(serialized, len(construct))
                serialized += construct
            else:
                raise InvalidTypeLength("Unknown param type {0}".format(param["type_length"]))
        else:
            # If the parameter is not primitive we recursively encode its type
            # until we find the primitive parameter
            construct = bytearray()
            self.serialize_param(construct, param, param_value)
            self.append_sdnv(serialized, len(construct))
            serialized += construct

    def encode_service(self, service):
        """
        Encodes a service
        :param service: Service to encode.
        :return: Serialized service.
        """
        serialized = bytearray()

        # First we serialize all the params
        construct = bytearray()
        for param_name, param_definition in service.definition["params"].iteritems():
            if param_definition["type"] not in self.types:
                raise MissingTypeDefinition("Missing type {0}, service can't be encoded."
                                            .format(param_definition["type"]))
            elif param_name.lower() not in service.fields:
                raise MissingParam(
                    "Missing param {0} in service configuration, service can't be encoded.".format(param_name.lower()))
            else:
                self.serialize_param(construct, param_definition, service.fields[param_name.lower()])

        service_id = service.definition["id"]
        serialized.append(int(service_id))
        self.append_sdnv(serialized, len(construct))
        serialized += construct

        return serialized

    def deserialize_types(self, serialized, _type, packed):
        """
        Deserialize a type.
        :param serialized: Serialized type.
        :param _type: Type to deserialize.
        :param packed: Optional parameter packed that determines if the type value is encoded.
        :return: Deserialized type.
        """
        off = 0

        # Get type defintiion
        type_definition = self.types[_type]

        # Skip type id
        off += 1

        if type_definition["type"] == "primitive":
            # If param is primitive decode it
            if type_definition["type_length"] == "fixed":
                fixed_length = int(type_definition["length"])
                param_value = serialized[off:off + fixed_length]
                off += fixed_length
            elif type_definition["type_length"] == "variable":
                param_value, param_len_length = sdnv_decode(serialized[off:])
                off += param_len_length
            elif type_definition["type_length"] == "explicit":
                param_len, param_len_length = sdnv_decode(serialized[off:])
                off += param_len_length
                # Special case string
                if _type == "string":
                    param_value = str(serialized[off:off + param_len])
                else:
                    param_value = serialized[off:off + param_len]
                off += param_len
            else:
                raise InvalidTypeDefinition("Unknown type length definition {0}".format(type_definition["type_length"]))

            # Unpack param value if necessary
            if packed == "ipv4":
                param_value = socket.inet_ntoa(buffer(param_value))
            elif packed == "ipv6":
                a = int(param_value) >> 64
                b = int(param_value) & ((1 << 64) - 1)
                param_value = socket.inet_ntop(socket.AF_INET6, struct.pack('!2Q', a, b))
            elif packed == "int":
                param_value = struct.unpack("!H", param_value)[0]

            type_field = param_value
        else:
            # Else recurse to find the primitive parameter
            # Get length
            param_len, param_len_length = sdnv_decode(serialized[off:])
            off += param_len_length
            # Get if it is packed
            if "pack" in type_definition:
                packed = type_definition["pack"]
            else:
                packed = None
            # Recursively decode all types
            type_len, type_field = self.deserialize_types(serialized[off:],
                                                          type_definition["type"],
                                                          packed)
            off += type_len

        return off, type_field

    def deserialize_param(self, serialized, param_definition, packed):
        """
        Deserializes a param
        :param serialized: Serialized param.
        :param param_definition: Definition of the serialized param.
        :param packed: Optional parameter packed that determines if the type value is encoded.
        :return: Deserialized param.
        """
        off, param_field = self.deserialize_types(serialized, param_definition["type"], packed)

        return off, param_field

    def decode_service(self, serialized, service_definition):
        """
        Decodes a service.
        :param serialized: Serialized service
        :param service_definition: Definition of the serialized service
        :return: Deserialized service.
        """
        service_fields = dict()
        off = 0

        # Get service length
        service_len, service_len_length = sdnv_decode(serialized[off:])
        off += service_len_length

        while off < service_len:
            # Get param id
            param_id = serialized[off]

            # Get param info
            for param_name, param_definition in service_definition["params"].iteritems():
                param_type = param_definition["type"]
                if param_type in self.types and self.types[param_type]["id"] == param_id:
                    break
            if not param_name or not param_definition:
                raise UnknownParam("Unknown param with id {0}".format(param_id))

            # Get if it is packed
            if "pack" in param_definition:
                packed = param_definition["pack"]
            else:
                packed = None

            # Decode param
            param_len, param_value = self.deserialize_param(serialized[off:], param_definition, packed)
            service_fields[param_name] = param_value
            off += param_len

        return service_fields, off
