# Copyright (C) 2015 TopCoder Inc., All Rights Reserved.

# Import modules so we don't have name conflicts.
import threading
import time
import struct
import re
import select
import socket
import logging
import sys
import copy

# Addition packages
import netaddr
import netifaces

# IPND dependencies
from beacon import *
from helper import GlobalVars


class Link(object):
    """
    Represents the link state of a neighbor
    """

    def __init__(self):
        self.is_up = False
        self.is_bidirectional = False

    def __str__(self):
        """
        String method
        :return: Link state as tring
        """
        return "Up ({0}), Bidirectional ({1})".format(self.is_up, self.is_bidirectional)


class LinkEvent(object):
    """
    Represents a link state change
    :param state: Link state, default DOWN
    """
    DOWN = 0
    UP = 1

    def __init__(self, state=DOWN):
        self.state = state


class Address(object):
    """
    Represents an IP address
    :param ip: IP address as an string
    """

    def __init__(self, ip):
        self.ip = netaddr.IPAddress(ip)
        self._setup()

    def _setup(self):
        """
        Sets up IP type (BROADCAST / UNICAST / MULTICAST)
        """
        if self.is_broadcast():
            self.type = "BROADCAST"
        elif self.ip.is_unicast():
            self.type = "UNICAST"
        elif self.is_multicast():
            self.type = "MULTICAST"
        else:
            raise Exception("Unrecognized address")

    def is_unicast(self):
        """
        Checks if IP is of type unicast
        :return: True if IP is unicast, False otherwise
        """
        return self.ip.is_unicast()

    def is_broadcast(self):
        """
        Checks if IP is of type broadcast
        :return: True if IP is broadcast, False otherwise
        """
        return self.ip == netaddr.IPAddress("255.255.255.255")

    def is_multicast(self):
        """
        Checks if IP is of type multicast
        :return: True if IP is multicast, False otherwise
        """
        return self.ip in netaddr.IPNetwork('224.0.0.0/24')

    def __eq__(self, other):
        """
        Compare method
        :param other: IP to compare
        :return: True if equal, False otherwise
        """
        return hasattr(other, "ip") and self.ip == other.ip

    def __hash__(self):
        """
        Hash method (p.e. used to insert addresses into dictionaries)
        :return: Address hash.
        """
        return hash(str(self.ip))

    def __str__(self):
        """
        String method
        :return: IP address as a string
        """
        return str(self.ip)


class Neighbor:
    """
    Represents a Neighbor
    :param address: Neighbor address
    :param _beacon: Last beacon received from this neighbor
    :param link: Link state with this neighbor
    """

    def __init__(self, address, _beacon=BeaconV4(), link=Link()):
        self.address = address
        self.beacon = _beacon
        self.link = link

    def __str__(self):
        """
        Neighbor string.
        :return: neighbor string
        """
        return "Address ({0}) Beacon ({1}) Link ({2})".format(self.address, self.beacon, self.link)

    def __eq__(self, other):
        """
        Compare method (neighbors get compared by their address)
        :param other: Neighbor to compare
        :return:  True if equal, False otherwise
        """
        return hasattr(other, "address") and self.address == other.address

    def __hash__(self):
        return hash(str(self.address))


class Node:
    """
    Represents an IPND node
    :param conf: IPNDConf class containing the configuration for the node.
    """

    # Maximum beacon size set to the maximum size of an UDP packet
    MAX_BEACON_SIZE = 65507

    def __init__(self, conf):
        self.conf = conf
        self.services = GlobalVars.IPND_ADMIN.get_all_services()
        self.sent_beacons = {}
        self.neighbors = {}
        self.destinations_lock = threading.Lock()
        # We need to create an instance of helper so it loads the TLV definitions
        # when it is created.
        self.helper = Helper()
        self._setup()
        self._main()

    def _setup(self):
        """
        Sets up the node from the provided configuration
        """
        # Set up beacon intervals
        self.beacon_intervals = {'BROADCAST': self.conf.beacon_interval_broadcast,
                                 'MULTICAST': self.conf.beacon_interval_multicast,
                                 'UNICAST': self.conf.beacon_interval_unicast}

        # Set up destinations
        self.destinations = set()
        if self.conf.multicast_discovery_address:
            self.destinations.add(self.conf.multicast_discovery_address)
        if self.conf.enumerated_nbs:
            for nb in self.conf.enumerated_nbs:
                self.destinations.add(nb)
        if self.conf.additional_destinations:
            for dest in self.conf.additional_destinations:
                self.destinations.add(dest)

        try:
            # Prepare sending socket
            self.sending_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        except socket.error as ex:
            logging.error("Can't prepare sending socket: {0}. Aborting execution.".format(ex))
            sys.exit(1)

        try:
            if self.conf.multicast_discovery_address:
                # Disable looping back the sent multicast datagrams to the host
                self.sending_socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 0)
                # Set up multicast TTL
                self.sending_socket.setsockopt(socket.IPPROTO_IP,
                                               socket.IP_MULTICAST_TTL,
                                               struct.pack('@i', self.conf.multicast_discovery_ttl))
            # Allow broadcast
            if self.conf.broadcast_discovery:
                self.sending_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        except socket.error as ex:
            logging.error("Can't setup sending socket options: {0}".format(ex))

        # Prepare receiving sockets
        self.receiving_sockets = []
        # We iterate over all the interfaces to bind to each one but not to loopback
        self.bind_addresses = []
        ifaces = netifaces.interfaces()
        for iface in ifaces:
            # Ignore loopback
            if re.match("lo*", iface):
                continue
            af_inet_addresses = netifaces.ifaddresses(iface)
            # Bind to all ipv4 addresses of the interface
            if netifaces.AF_INET not in af_inet_addresses:
                continue
            for addrs in af_inet_addresses[netifaces.AF_INET]:
                if 'addr' in addrs:
                    addr = addrs['addr']
                else:
                    continue

                try:
                    # Create socket
                    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    # Bind to address and port
                    sock.bind((addr, self.conf.port))
                    if self.conf.multicast_discovery_address:
                        # Join multicast group in this interface
                        mreq = socket.inet_aton(str(self.conf.multicast_discovery_address)) + socket.inet_aton(addr)
                        sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
                    if self.conf.broadcast_discovery:
                        # Allow reception of broadcast packets from this socket
                        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

                    logging.info("Bind to {0} successful".format(addr))
                    self.bind_addresses.append(addr)
                    self.receiving_sockets.append(sock)
                except socket.error as ex:
                    logging.error("Can't prepare socket for {0}: {1}".format(addr, ex))

        if self.conf.multicast_discovery_address:
            try:
                # Prepare multicast socket
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                # setsockopt() information: http://man7.org/linux/man-pages/man7/socket.7.html
                # Allow multiple binds to the same SOCK_DGRAM UDP port
                sock.setsockopt(socket.IPPROTO_IP, socket.SO_REUSEADDR, 1)
                # Configure TTL of the multicast datagrams
                sock.setsockopt(socket.IPPROTO_IP,
                                socket.IP_MULTICAST_TTL,
                                struct.pack('@i', self.conf.multicast_discovery_ttl))
                # Bind
                sock.bind((str(self.conf.multicast_discovery_address.ip), self.conf.port))
                # Append socket to list of receiving sockets
                self.receiving_sockets.append(sock)
            except socket.error as ex:
                logging.error("Can't prepare multicast receiving socket for {0}: {1}"
                              .format(self.conf.multicast_discovery_address, ex))

        if self.conf.broadcast_discovery:
            try:
                # Prepare broadcast socket
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                # setsockopt() information: http://man7.org/linux/man-pages/man7/socket.7.html
                # Allow multiple binds to the same SOCK_DGRAM UDP port
                sock.setsockopt(socket.IPPROTO_IP, socket.SO_REUSEADDR, 1)
                # Bind
                sock.bind(("255.255.255.255", self.conf.port))
                # Append socket to list of receiving sockets
                self.receiving_sockets.append(sock)
            except socket.error as ex:
                logging.error("Can't prepare multicast receiving socket for {0}: {1}"
                              .format(self.conf.multicast_discovery_address, ex))

    def send_beacon_worker(self):
        """
        Intended to run as a thread. Sends beacons periodically
        """
        # Send out beacons on node.port at periodic intervals.
        logging.info("Send beacon worker started")

        min_remaining_period = None
        while GlobalVars.ALIVE:
            # In future, beacon intervals may be updated through IPNDAdmin, so re-compute it on each thread run.
            if min_remaining_period:
                beacon_interval = min(self.beacon_intervals.values(), min_remaining_period)
            else:
                beacon_interval = min(self.beacon_intervals.values())

            time.sleep(beacon_interval)

            min_remaining_period = min(self.beacon_intervals.values())

            # Create beacon
            _beacon = BeaconV4()

            # Populate beacon
            # Configure beacon from IPND configuration
            _beacon.from_conf(conf=self.conf)
            # Add services
            _beacon.add_services(self.services)
            # Update nbf
            _beacon.update_nbf(self.neighbors)

            self.destinations_lock.acquire()
            for address in self.destinations:
                logging.debug("Starting beacon sending to {0}".format(address))

                # Get previously sent beacon
                sent_beacon = None
                if address in self.sent_beacons:
                    sent_beacon = self.sent_beacons[address]
                    logging.debug("Found previously sent beacon: {0}".format(sent_beacon))

                # If it is the first beacon that we send, just send it
                if not sent_beacon:
                    logging.debug("Previously sent beacon not found.")

                    # Prepare beacon for this destination
                    _beacon.period = self.beacon_intervals[address.type]

                    # Send beacon
                    self.send_beacon(copy.deepcopy(_beacon), address)

                # else check if the period has expired to know if we have to send another one
                elif int(time.time()) >= (sent_beacon.send_time + self.beacon_intervals[address.type]):
                    logging.debug("Period expired")

                    # Prepare beacon for this destination
                    _beacon.period = self.beacon_intervals[address.type]
                    _beacon.seq_number = sent_beacon.seq_number + 1

                    # If beacon is different than the previous sent beacon or
                    # node does not have an active connection with this address,
                    # send the beacon
                    if self.helper.diff_beacons(_beacon, sent_beacon):
                        logging.debug("Beacon has changed")
                        # Send beacon
                        self.send_beacon(copy.deepcopy(_beacon), address)
                    elif not self.has_active_connection(address):
                        logging.debug("Node does not have an active connection with {0}".format(address))
                        # ["2.4. Allowing Data to Substitute for Beacons", page 6]
                        # Send beacon
                        self.send_beacon(copy.deepcopy(_beacon), address)
                else:
                    logging.debug("Period still not expired")

                    remaining_period = (sent_beacon.send_time + self.beacon_intervals[address.type]) - time.time()
                    min_remaining_period = min(min_remaining_period, remaining_period)
            self.destinations_lock.release()

        # Clean exit
        self.sending_socket.close()

    def send_beacon(self, _beacon, address):
        """
        Sends a beacon to address.
        :param _beacon: Beacon to send
        :param address: Beacon destination
        """

        _beacon.send_time = int(time.time())
        serialized = self.helper.encode_beacon(_beacon)

        try:
            self.sending_socket.sendto(serialized, (str(address), self.conf.port))
        except Exception as ex:
            logging.error("Beacon ({0}B) sending to {1} failed! {2}".format(len(serialized), address.ip, ex))
        else:
            logging.info("Beacon ({0}B) sending to {1} succeeded.".format(len(serialized), address))
            logging.debug("Beacon ({0}B) sent to {1}: {2}".format(len(serialized), address,_beacon))
        finally:
            self.sent_beacons[address] = _beacon

    def has_active_connection(self, address):
        """
        Placeholder method that checks if address has an active connection with the node
        :param address: Address to check for an active connection
        :return: True if address ahs an active connection, False otherwise.
        """
        if not address.is_unicast():
            return False
        else:
            # For the moment we return always False.
            # We should check with the DTN platform if address has an active connection
            return False

    def check_udp_checksum(self, pkt):
        """
        Checks if UDP checksum of packet is correct.
        :param pkt: packet to check
        :return: True if UDP checksum is correct, False otherwise
        """

        return True

    def receive_beacon_worker(self):
        """
        Intended to run as a thread. Listens on all interfaces for beacons.
        """
        # Bind on all addresses (on all network interfaces) but not on the loopback address.
        logging.info("Receive beacon worker started")

        while GlobalVars.ALIVE:
            timeout = min(self.beacon_intervals.values())

            readable = select.select(self.receiving_sockets, [], [], timeout)[0]
            for sock in readable:
                data = bytearray(self.MAX_BEACON_SIZE)
                data_len, addr = sock.recvfrom_into(data, self.MAX_BEACON_SIZE)
                recv_time = time.time()
                sender_addr = Address(addr[0])
                recv_addr = Address(sock.getsockname()[0])

                # We ignore packets sent from an unicast address which we are bind:
                if addr[0] in self.bind_addresses:
                    continue

                logging.info("Data received ({0}B) from {1}:{2} at {3}"
                             .format(data_len, sender_addr, addr[1],
                                     time.strftime("%d %b %Y %H:%M:%S", time.gmtime(recv_time))))

                if sender_addr in self.neighbors:
                    sender = self.neighbors[sender_addr]
                    logging.info("Sender {0} is a known neighbor".format(sender.address))
                    logging.debug("Previous neighbor information: {0}".format(sender))
                else:
                    logging.info("Sender {0} is an unknown neighbor".format(sender_addr))
                    sender = Neighbor(sender_addr)

                _beacon = self.helper.decode_beacon(data, data_len)

                if _beacon:
                    logging.info("Received data is a beacon")
                    logging.debug("Beacon contents: {0}".format(_beacon))

                    # ["2.6. Beacon Message Format", page 7]:
                    # "An IPND node MUST use UDP checksums to ensure correctness."
                    if not self.check_udp_checksum(data):
                        print "Received beacon with an incorrect UDP checksum"
                        continue
                    
                    if not _beacon.period:
                        # Set a period for the beacon if it does not contain one so we can calculate when the 
                        # neighbor has expired.
                        _beacon.period = self.beacon_intervals[recv_addr.type]

                    sender.beacon = _beacon
                    if sender.beacon:
                        if sender.beacon.nbf and sender.beacon.nbf.contains(str(self.conf.eid)):
                            logging.info("Our EID is present inside nb NBF, link is bidirectional")
                            sender.link.is_bidirectional = True
                        else:
                            logging.info("Our EID is not present inside nb NBF, link is not bidirectional")
                            sender.link.is_bidirectional = False
                    else:
                        logging.info("NBF not present, we can't know if link is bidirectional.")

                else:
                    logging.info("Received data ({0}B) is not a beacon.".format(data_len))
                    logging.debug("Data content: {0}".format(data_len))
                    if recv_addr.is_unicast():
                        # Sender address is unicast therefore we consider that the received data
                        # contains a NBF which indicates our membership to the 1-hop neighborhood
                        # of the sender
                        logging.info("Destination addr is unicast, link is bidirectional.")
                        sender.beacon.period = self.beacon_intervals[sender_addr.type]
                        sender.link.is_bidirectional = True
                    elif recv_addr.is_broadcast() or recv_addr.is_multicast():
                        # Sender addr is not unicast
                        logging.info("Destination addr is not unicast, getting old beacon information if present.")
                        if sender_addr in self.neighbors:
                            logging.info("Old beacon information found.")
                            old_sender = self.neighbors[sender_addr]
                            # Therefore the link is bidirectional only if it previously was
                            sender.link = old_sender.link
                            # And we presume that the previous beacon has not changed
                            sender.beacon = old_sender.beacon
                            sender.beacon.seq_number += 1
                        else:
                            logging.info("Old beacon information not found.")
                            sender.beacon.period = self.beacon_intervals[sender_addr.type]

                # ["2.1. Beacon Period", paragraph 3, pages 5 - 6]
                sender.beacon.receive_time = recv_time

                if time.time() < sender.beacon.receive_time + self.beacon_intervals[sender_addr.type]:
                    sender.link.is_up = True

                new_nb = False
                if sender_addr not in self.neighbors:
                    new_nb = True
                # Update nb state
                self.neighbors[sender_addr] = sender
                # Add new destination (if it wasn't already)
                self.destinations_lock.acquire()
                self.destinations.add(sender_addr)
                self.destinations_lock.release()

                link_event = LinkEvent()
                if sender.link.is_up:
                    link_event.state = LinkEvent.UP
                else:
                    link_event.state = LinkEvent.DOWN

                if sender.link.is_up and new_nb:
                    # Announce the presence of this newly discovered neighbor.
                    # ["2.7. IPND and CLAs", paragraph 2, page 16]
                    self.notify_cla(sender, link_event)

                elif not sender.link.is_up:
                    # Notify dependent CLAs that this neighbor's link has gone down.
                    # ["2.7. IPND and CLAs", paragraph 3, page 16]
                    self.notify_cla(sender, link_event)

        # Clean exit
        for sock in self.receiving_sockets:
            sock.close()

    def notify_cla(self, neighbor, link_event):
        """
        Notifies CLA a neighbor link state change.
        :param neighbor: Neighbor which changed link state
        :param link_event: Link state event
        """
        if link_event.state == LinkEvent.DOWN:
            logging.info("CLA notified that link {0} has gone down".format(neighbor.address))
        else:
            logging.info("CLA notified that link {0} is up".format(neighbor.address))
        pass

    def expire_beacon_worker(self):
        """
        Intended to run as a thread. Cleans neighbors periodically.
        """
        # Purge the neighbors set of node whose beacon intervals have expired.
        # See ["2.8. Disconnection", paragraph 1, page 16]
        logging.info("Expire beacon worker started")

        while GlobalVars.ALIVE:
            beacon_interval = max(self.beacon_intervals.values())

            time.sleep(beacon_interval)
            logging.info("Starting expired neighbor cleaning")

            for nb in self.neighbors.copy().itervalues():
                # If we have not received a beacon in nb.beacon.period
                # Neighbor has disappeared and therefore link has gone down
                if time.time() > (nb.beacon.receive_time + nb.beacon.period):
                    logging.info("Beacon not received in {0} seconds, neighbor {1} has disappeared"
                                 .format(nb.beacon.period, nb.address))
                    link_event = LinkEvent(LinkEvent.DOWN)
                    self.notify_cla(nb, link_event)
                    if nb.address.is_unicast():
                        del self.neighbors[nb.address]
                        if nb.address in self.destinations:
                            self.destinations_lock.acquire()
                            self.destinations.remove(nb.address)
                            self.destinations_lock.release()

    def start(self):
        """
        Starts all threads.
        """
        self.beacon_sent_worker.start()
        self.beacon_recv_worker.start()
        self.beacon_expired_worker.start()

    def join(self):
        """
        Joins all threads (waits for threads to finish).
        """
        self.beacon_sent_worker.join()
        self.beacon_recv_worker.join()
        self.beacon_expired_worker.join()

    def _main(self):
        """
        Prepares all threads
        """
        self.beacon_sent_worker = threading.Thread(target=self.send_beacon_worker)
        self.beacon_recv_worker = threading.Thread(target=self.receive_beacon_worker)
        self.beacon_expired_worker = threading.Thread(target=self.expire_beacon_worker)
