""" Holds configuration parameters common to both pavement.py and test.py. Avoids circular import"""
from config import config, envs

UBUNTU_MIRROR = envs.get('UBUNTU_MIRROR', 'archive.ubuntu.com')

DOCKER = config.get("DOCKER", 'docker')
DOCKER_ADDR = config.get("DOCKER_VNET", '10.0.0.1/16')
DOCKER_SERVICE = config.get("DOCKER_SERVICE", 'docker')

TAG_NS = 'dtn_test'

DTN2_NODE_NAME = config.get("DTN2_NODE_NAME", 'node1')
DTN2_NODE_IP = config.get("DTN2_NODE_IP", '10.0.1.1')

IBR_NODE_NAME = config.get("IBR_NODE_NAME", 'node2')
IBR_NODE_IP = config.get("IBR_NODE_IP", '10.0.1.2')

IPND_NODE_NAME = config.get("IPND_NODE_NAME", 'node3')
IPND_NODE_IP = config.get("IPND_NODE_IP", '10.0.1.3')

ION_NODE_NAME = config.get("ION_NODE_NAME", 'node4')
ION_NODE_IP = config.get("ION_NODE_IP", '10.0.1.4')