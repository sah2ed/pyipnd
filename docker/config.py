# This configuration file must be a valid Python source file and contains dictionaries. Do not forget to wrap
# strings in quotes and the comma after value.

envs = {
    # Environment variables passed to Docker containers

    # Ubuntu Mirror. Pick the closest one to you. Usually just change the country code on the beginning of the address.
    # If unsure, you can use the default "archive.ubuntu.com".
    'UBUNTU_MIRROR': 'br.archive.ubuntu.com',

    # The ION DTN implementation download URL. Tested with 3.3.0 version
    'ION_DOWNLOAD_URL': "http://ufpr.dl.sourceforge.net/project/ion-dtn/ion-3.3.0.tar.gz",

    # The DTN2-OASYS revision to download from SourceForge's mercurial system
    'OASYS_REVISION': "0838b1c4880bc4df29ec859ca5e0a6ac4361e1c5",

    # The DTN2-OASYS mercurial clone URL
    'OASYS_CLONE_URL': "http://hg.code.sf.net/p/dtn/oasys",

    # The DTN2 revision to download from SourceForge's mercurial system
    'DTN2_REVISION': "326755a3af4372d7209436ef5673696ccfce3200",

    # The DTN2 mercurial clone URL
    'DTN2_CLONE_URL': "http://hg.code.sf.net/p/dtn/DTN2",

    # The DTN2 options passed to the dtnd command
    'DTN2_OPTS': '-c /etc/dtn2/dtn_multicast.conf -l info -o /var/log/dtn2.log',

    # The IBR distribution URL
    'IBR_DIST_URL': 'http://download.opensuse.org/repositories/home:/j_morgenroth/xUbuntu_14.04',

    # The IBR version to download. Must be available in the provided repository
    'IBR_VERSION': '1.0.1',

    # The IBR options passed to the dtnd command
    'IBR_OPTS': '-c /etc/ibrdtn/ibrdtnd_multicast.conf',

    # The IPND options passed to IPND
    'IPND_OPTS':''

    }


config = {
    # Build script configuration variables.

    # Location of the Docker executable. Make sure you don't need superuser privileges to run Docker.
    'DOCKER': '/usr/bin/docker',

    # The Docker virtual network address and subnet configuration. You may change if it conflicts with your
    # LAN configuration. If you change from the default 10.0.0.250/16, check if you need to update the config files
    'DOCKER_ADDR': '10.0.0.1/16',

    # The Docker local service name that will be started/stoped with "service <name> start" or "service <name> stop"
    'DOCKER_SERVICE': 'docker',

    ### DTN2 related configuration ###

    # Name of the Docker container for this implementation
    'DTN2_NODE_NAME': 'node1',

    # IP address of the container. Must be within the DOCKER_ADDR subnet
    'DTN2_NODE_IP': '10.0.1.1/16',

    ### IBR related configuration ###

    # Name of the Docker container for this implementation
    'IBR_NODE_NAME': 'node2',

    # IP address of the container. Must be within the DOCKER_ADDR subnet
    'IBR_NODE_IP': '10.0.1.2/16',

    ### IPND related configuration ###

    # Name of the Docker container for this implementation
    'IPND_NODE_NAME': 'node3',

    # IP address of the container. Must be within the DOCKER_ADDR subnet
    'IPND_NODE_IP': '10.0.1.3/16',

    ### DTN-ION related configuration ###

    # Name of the Docker container for this implementation
    'ION_NODE_NAME': 'node4',

    # IP address of the container. Must be within the DOCKER_ADDR subnet
    'ION_NODE_IP': '10.0.1.4/16',

}