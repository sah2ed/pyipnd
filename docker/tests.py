from unittest import *
from subprocess import check_call, check_output, call
from context import *
import ipcalc
import socket
import os
import time

""":type : pavement"""


### Test Case ###
class DTNTest(TestCase):

    def test_1_containers_up(self):
        """Test if containers are up and runnning"""
        def ps_count(container):
            return int(check_output("%s ps | grep %s | wc -l" % (DOCKER, container), shell=True).strip())

        self.assertEqual(ps_count(DTN2_NODE_NAME), 1)
        self.assertEqual(ps_count(ION_NODE_NAME), 1)
        self.assertEqual(ps_count(IBR_NODE_NAME), 1)


    def test_2_interconnection(self):
        """ Test if containers can ping each other """
        FNULL = open(os.devnull, 'w')

        def ping(node_name, destination):
            cmd = "%s exec %s ping -c1 -W2 %s" % (DOCKER, node_name, ip(destination))
            ret = call(cmd, shell=True, stdout=FNULL)
            msg = "Ping to %s from container %s returned %s" % (ip(destination), node_name, ret)
            self.assertEqual(ret, 0, msg)
            return ret

        def ip(value):
            return str(ipcalc.IP(value))


        # DTN2: ping ION and IBR and docker0
        ping(DTN2_NODE_NAME, ION_NODE_IP)
        ping(DTN2_NODE_NAME, IBR_NODE_IP)
        ping(DTN2_NODE_NAME, DOCKER_ADDR)

        # ION: DTN2 and IBR and docker0
        ping(ION_NODE_NAME, DTN2_NODE_IP)
        ping(ION_NODE_NAME, IBR_NODE_IP)
        ping(ION_NODE_NAME, DOCKER_ADDR)

        # IBR: ION and DTN2 and docker0
        ping(IBR_NODE_NAME, DTN2_NODE_IP)
        ping(IBR_NODE_NAME, ION_NODE_IP)
        ping(IBR_NODE_NAME, DOCKER_ADDR)


    def test_3_beacon_received(self):
        """Test if the DTN2 and IBR are sending beacons to the network. Wait 20s before timeout."""

        # The DTN2 and IBR implementations implement different versions of the IPND beacon protocol - version
        # 0x04 and 0x02 respectively. Thus, they're currently not interoperable. So we're just going to check
        # if they're sending multicast beacons and that the data received contains their EID.

        # listen to multicast UDP packets in group 224.0.0.1 port 4551

        GROUP = '224.0.0.1'
        PORT = 4551
        docker0 = str(ipcalc.IP(DOCKER_ADDR))  # get ip of local docker interface

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)
        sock.bind(('', PORT))

        # add membership to multicast group
        mreq = socket.inet_aton(GROUP) + socket.inet_aton(docker0)
        sock.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP, mreq)

        # Check if we can receive multicast beacon data from both DTN2 and IBR nodes. By default, the nodes
        # set the hostname as part of the EID string, so we'll look for that.
        # Set a 20 second timeout

        sock.settimeout(20.0)
        start_time = time.time()
        found_dtn2 = False
        found_ibr = False
        while time.time() < start_time + 20.0:
            try:
                data, sender = sock.recvfrom(1024)

                if DTN2_NODE_NAME in data:
                    found_dtn2 = True

                if IBR_NODE_NAME in data:
                    found_ibr = True

                if found_ibr and found_dtn2:
                    break

            except socket.timeout:
                raise Exception("Timeout waiting for beacons.")

        self.assertTrue(found_dtn2, msg="Should've received beacon from DTN2 node")
        self.assertTrue(found_ibr, msg="Should've received beacon from IBR node")
