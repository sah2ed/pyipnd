from paver.easy import *
from subprocess import check_call, check_output
import time
import os
import re
import ipcalc
import unittest
import tests
from context import *

try:
    from jinja2 import Template
except ImportError:
    print "You did not install Jinja2. Please check the README.txt file"

### Tasks ###

@task
def dockerfile_ion():
    """Creates dockerfile for DTN-ION"""
    env_keys = ['ION_DOWNLOAD_URL']
    create_dockerfile('ion', env_keys)

@task
def dockerfile_dtn2():
    """Creates dockerfile for DTN2"""
    env_keys = ['OASYS_REVISION', 'OASYS_CLONE_URL', 'DTN2_REVISION', 'DTN2_CLONE_URL']
    create_dockerfile('dtn2', env_keys)

@task
def dockerfile_ibr():
    """Creates dockerfile for IBR"""
    env_keys = ['IBR_DIST_URL', 'IBR_VERSION']
    create_dockerfile('ibr', env_keys)

@task
def dockerfile_ipnd():
    """Creates dockerfile for IPND"""
    env_keys = []
    create_dockerfile('ipnd', env_keys)

@task
@needs(['dockerfile_ion', 'dockerfile_dtn2', 'dockerfile_ibr', 'dockerfile_ipnd'])
def dockerfile_all():
    """Creates dockerfiles for all implementations"""
    pass

@task
@needs(['dockerfile_ion'])
def build_ion():
    """Build Docker image for DTN-ION"""
    build_image('ion')

@task
@needs(['dockerfile_dtn2'])
def build_dtn2():
    """Build Docker image for DTN2"""
    build_image('dtn2')

@task
@needs(['dockerfile_ibr'])
def build_ibr():
    """Build Docker image for IBR"""
    build_image('ibr')

@task
@needs(['dockerfile_ipnd'])
def build_ipnd():
    """Build Docker image for IBR"""
    # Get latest IPND version
    sh("cp -R ../IPND support/IPND/")
    build_image('ipnd')

@task
@needs(['build_ion', 'build_dtn2', 'build_ibr', 'build_ipnd'])
def build_all():
    """Build Docker images for all implementations """
    pass

@task
def vnet_setup():
    """Setup the Docker virtual network to the range configured in the config.py file"""
    setup_docker_vnet(DOCKER_ADDR)
    time.sleep(2)


@task
def vnet_restore():
    """Restore the Docker virtual network to the default 172.17.42.1/16"""
    setup_docker_vnet('172.17.42.1/16')
    time.sleep(2)


@task
@needs(['vnet_setup'])
def run_ion():
    """Start a container with the ION implementation"""
    run_docker('ion', ION_NODE_NAME, ION_NODE_IP)
    time.sleep(2)

@task
@needs(['vnet_setup'])
def run_dtn2():
    """Start a container with the DTN2 implementation"""
    my_envs = {
        'DTN2_OPTS': envs.get('DTN2_OPTS', '-l debug -o /var/log/dtn2.log')
    }
    run_docker('dtn2', DTN2_NODE_NAME, DTN2_NODE_IP, envs=my_envs)
    time.sleep(2)

@task
@needs(['vnet_setup'])
def run_ibr():
    """Start a container with the DTN2 implementation"""
    my_envs = {
        'IBR_OPTS': envs.get('IBR_OPTS', '-l debug -o /var/log/dtn2.log')
    }
    run_docker('ibr', IBR_NODE_NAME, IBR_NODE_IP, envs=my_envs)
    time.sleep(2)

@task
@needs(['vnet_setup'])
def run_ipnd():
    """Start a container with the IPND implementation"""
    run_docker('ipnd', IPND_NODE_NAME, IPND_NODE_IP)
    time.sleep(2)


@task
@needs(['run_ion', 'run_ibr', 'run_dtn2', 'run_ipnd'])
def run_all():
    """Start all Docker containers"""
    pass

@task
def stop_ion():
    """Stop the ION container"""
    sh("%s stop %s" % (DOCKER, ION_NODE_NAME), ignore_error=True)

@task
def stop_ibr():
    """Stop the IBR container"""
    sh("%s stop %s" % (DOCKER, IBR_NODE_NAME), ignore_error=True)

@task
def stop_dtn2():
    """Stop the DTN2 container"""
    sh("%s stop %s" % (DOCKER, DTN2_NODE_NAME), ignore_error=True)

@task
def stop_ipnd():
    """Stop the IPND container"""
    sh("%s stop %s" % (DOCKER, IPND_NODE_NAME), ignore_error=True)

@task
@needs(['stop_ion', 'stop_ibr', 'stop_dtn2', 'stop_ipnd'])
def stop_all():
    """Stop all containers"""
    pass

@task
def remove_ion():
    """Forcefully remove a ION container if exists"""
    sh("%s rm -f %s" % (DOCKER, ION_NODE_NAME), ignore_error=True)

@task
def remove_ibr():
    """Forcefully remove a IBR container if exists"""
    sh("%s rm -f %s" % (DOCKER, IBR_NODE_NAME), ignore_error=True)

@task
def remove_dtn2():
    """Forcefully remove a DTN2 container if exists"""
    sh("%s rm -f %s" % (DOCKER, DTN2_NODE_NAME), ignore_error=True)

@task
def remove_ipnd():
    """Forcefully remove a IPND container if exists"""
    sh("%s rm -f %s" % (DOCKER, IPND_NODE_NAME), ignore_error=True)

@task
@needs(['remove_ion','remove_ibr','remove_dtn2', 'remove_ipnd'])
def remove_all():
    """Forcefully remove a all containers if exist"""
    pass

@task
def test():
    """Run tests. Requires all running containers."""
    suite = unittest.makeSuite(tests.DTNTest)
    result = unittest.TextTestRunner(verbosity=2).run(suite)
    if result.failures or result.errors:
        print ""
        print "You had a test failure. At a minimum, check if you build all the containers with the 'build_all' " \
              "command and that they're stared with the 'run_all' command. If the containers are already started, " \
              "try removing them before starting with the 'remove_all' command."

@task
def clean():
    """Clean build directory"""
    sh('rm ./build -rf')


### Helpers ###

def tag(impl):
    return '%s/%s' % (TAG_NS, impl)

def create_dockerfile(impl, env_keys):
    tpl_filename = 'templates/%s.dockerfile' % impl
    out_filename = 'build/%s.dockerfile' % impl

    my_envs = {}

    for key in env_keys:
        my_envs[key] = envs[key]

    if not os.path.isdir('build'):
        os.mkdir('build')

    with open(tpl_filename) as tpl_file:
        tpl = Template(tpl_file.read())

    with open(out_filename, 'w') as out_file:
        out_file.write(tpl.render(ubuntu_mirror=UBUNTU_MIRROR, envs=my_envs))

def build_image(impl):
    sh("%s build -t %s -f build/%s.dockerfile ." %(DOCKER, tag(impl), impl))

    print ''
    print 'Image created with tag "%s"' % tag(impl)

def is_docker_running():
    try:
        check_output("docker info", shell=True)
        return True
    except:
        return False

def setup_docker_vnet(vnet):
    cur_vnet = check_output("ip addr show dev docker0 | grep 'inet '| awk '{print $2}'", shell=True).strip()
    if cur_vnet == vnet:
        return

    # turn off docker if started
    is_running = is_docker_running()

    if is_running:
        sh("sudo service %s stop" % DOCKER_SERVICE)

    # update vnet
    sh("sudo ifconfig docker0 %s" % vnet)

    # remove iptables rules
    remove_docker_iptables_rules(cur_vnet)

    # start if was running
    if is_running:
        sh("sudo service %s start" % DOCKER_SERVICE)

def get_container_state(node_name):
    """
    :return: either RUNNING, STOPPED, NOT_FOUND
    """
    try:
        resp = check_output("docker inspect -f '{{ .State.Running }}' " % node_name).strip()
        if resp == 'true':
            return 'RUNNING'
        else:
            return 'STOPPED'
    except:
        return 'NOT_FOUND'


def run_docker(impl, node_name, node_ip, envs=None):
    # Check if docker is running
    if not is_docker_running():
        print "Docker is not running, trying to start the service."
        sh("sudo service %s start", DOCKER_SERVICE)

    # Check if there's a running container with the same name
    state = get_container_state(node_name)

    if state != 'NOT_FOUND':
        # Remove any existing container
        sh("%s rm -f %s" %(DOCKER, node_name))

    gateway = str(ipcalc.IP(DOCKER_ADDR))

    my_envs = {}
    if envs:
        my_envs.update(envs)

    my_envs.update({
        'DOCKER_IP': node_ip,
        'DOCKER_GATEWAY': gateway
    })

    run_cmd = [DOCKER, 'run',
               '-t',
               '-d',
               '--privileged=true',
               '--name', node_name,
               '--hostname', node_name]

    for key, val in my_envs.iteritems():
        run_cmd.append('-e')
        run_cmd.append('%s="%s"' % (key, val))

    run_cmd.append(tag(impl))

    sh(' '.join(run_cmd))
    time.sleep(3)

    print ''
    print 'Started a container with impl "%s" named "%s" with IP "%s"' % (impl, node_name, node_ip)

def remove_docker_iptables_rules(vnet):
    """Remove iptable rules created by Docker. They'll be recreated on Docker start"""

    net = ipcalc.Network(vnet)
    vnet = str(net.network()) + '/' + str(net.mask)

    ret = check_output('sudo iptables -t nat -L --line-numbers', shell=True).splitlines()

    chain = None
    for line in ret:
        pattern_chain = 'Chain (.*) \('
        match_chain = re.match(pattern_chain, line)

        pattern_rule = '^(\d*) +(\S+) +(\S+) +(\S+) +(\S+) +(\S+) +(.*)$'
        match_rule = re.match(pattern_rule, line)

        if match_chain:
            chain = match_chain.group(1)
            if chain == 'DOCKER':
                check_call('sudo iptables -t nat -X %s' % chain, shell=True)

        elif match_rule:
            num, target, prot, opt, source, destination, args = match_rule.groups()
            if target == 'DOCKER' or (target == 'MASQUERADE' and source == vnet):
                check_call('sudo iptables -t nat -D %s %s' % (chain, num), shell=True)

        else:
            pass