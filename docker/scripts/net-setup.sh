#!/bin/bash

# This is a script to setup networking on a Docker container prior to executing the main program. It relies on the
# environment variables "DOCKER_IP" and "DOCKER_GATEWAY". To use it in a Dockerfile, set your as follows:
#
# ADD net-setup.sh /tmp/net-setup.sh
# RUN chmod +x /tmp/net-setup.sh
# CMD /tmp/net-setup.sh && <your commands>
#
# You can pass the environment variables with the "docker run -e" switch. You also have to use the '--privileged'
# switch to allow a container to change it's own IP.

set -e

# Check if the environment variables exist. Else we should warn, cleanly exit and keep existing configuration.
if [ "$DOCKER_IP" == "" ] || [ "$DOCKER_GATEWAY" == "" ]; then
    print "[WARN] DOCKER_IP or DOCKER_GATEWAY were not defined. Exiting without any change"
    exit 0
fi

# Change ip
ifconfig eth0 $DOCKER_IP

# Restore default route if needed
HAS_DEFAULT_ROUTE=$(ip route show | grep 'default via' | wc -l)

if [ "$HAS_DEFAULT_ROUTE" == "0" ]; then
    ip route add default via $DOCKER_GATEWAY
fi

sleep 3