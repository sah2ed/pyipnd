FROM ubuntu:14.04
MAINTAINER TCSDEVELOPER

# replace sh by bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# set ubuntu mirror
ENV UBUNTU_MIRROR {{ubuntu_mirror}}

# change sources.list to closest mirror and update package info
RUN sed -i "s/http:\/\/archive.ubuntu.com/http:\/\/$UBUNTU_MIRROR/g" /etc/apt/sources.list
RUN apt-get -y update
RUN apt-get upgrade -y

# install build-essentail expat mercurial tcl-dev
RUN apt-get install build-essential libexpat1-dev mercurial tcl-dev -y

# install additional libraries
RUN apt-get install libgoogle-perftools-dev libxerces-c2-dev libdb5.1-dev xsdcxx libmysql++-dev libpq-dev \
    libreadline-dev tcl-tclreadline -y

# set application environment variables
{% for key, value in envs.items() %}
ENV {{key}} {{value}}
{% endfor %}

# clone oasys
WORKDIR /opt
RUN hg clone -u $OASYS_REVISION $OASYS_CLONE_URL oasys
RUN hg clone -u $DTN2_REVISION $DTN2_CLONE_URL dtn2

# compile oasys
WORKDIR /opt/oasys
RUN ./configure --with-python --with-google-perftools --with-xsd-tool=xsdcxx --with-expat --with-xerces-c --with-tcl \
    --with-tclreadline=no
RUN make

# compile dtn2 with IPND
WORKDIR /opt/dtn2
# Copy Foorouter service patch
ADD support/DTN2/FooRouterPatch/* servlib/discovery/ipnd_srvc/
RUN ./configure --with-bbn-ipnd
RUN make
RUN make install

# install the configuration files
RUN mkdir /etc/dtn2/
ADD support/DTN2/*.conf /etc/dtn2/

# initialize datastore
RUN dtnd -c /etc/dtn2/dtn_multicast.conf --init-db

# add net-setup
ADD scripts/net-setup.sh /tmp/net-setup.sh
RUN chmod +x /tmp/net-setup.sh

# run the service
CMD /tmp/net-setup.sh && dtnd $DTN2_OPTS
#end