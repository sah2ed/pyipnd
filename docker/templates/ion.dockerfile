FROM ubuntu:14.04
MAINTAINER TCSDEVELOPER

# replace sh by bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# set ubuntu mirror
ENV UBUNTU_MIRROR {{ubuntu_mirror}}

# change sources.list to closest mirror and update package info
RUN sed -i "s/http:\/\/archive.ubuntu.com/http:\/\/$UBUNTU_MIRROR/g" /etc/apt/sources.list
RUN apt-get -y update
RUN apt-get upgrade -y

# install build-essentail expat and wget
RUN apt-get install build-essential libexpat1-dev wget -y

# create and set the working dir
RUN mkdir -p /opt/ion

# Create a symlink from make to "gmake"
RUN ln -s /usr/bin/make /usr/bin/gmake

# set environment variables
{% for key, value in envs.items() %}
ENV {{key}} {{value}}
{% endfor %}

# download and unzip the into workdir
RUN wget $ION_DOWNLOAD_URL -P /tmp
RUN tar -xzf /tmp/$(basename $ION_DOWNLOAD_URL) -C /opt/ion

WORKDIR /opt/ion/ion-open-source

RUN ./configure

# make and install modules
RUN make
RUN make install
RUN ldconfig

# add local configuration file
ADD support/ION/ion.rc /etc/ion.rc

# add net-setup
ADD scripts/net-setup.sh /tmp/net-setup.sh
RUN chmod +x /tmp/net-setup.sh

# Run ION
WORKDIR /
CMD /tmp/net-setup.sh && ionstart -I /etc/ion.rc; read -p "Press any key to stop."