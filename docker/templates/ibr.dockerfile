FROM ubuntu:14.04
MAINTAINER TCSDEVELOPER

# replace sh by bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# set ubuntu mirror
ENV UBUNTU_MIRROR {{ubuntu_mirror}}


# change sources.list to closest mirror and update package info
RUN sed -i "s/http:\/\/archive.ubuntu.com/http:\/\/$UBUNTU_MIRROR/g" /etc/apt/sources.list
RUN apt-get -y update
RUN apt-get upgrade -y

# install wget
RUN apt-get install wget -y

# set environment variables
{% for key, value in envs.items() %}
ENV {{key}} {{value}}
{% endfor %}

# install IBR from custom Ubuntu repository
RUN DEBIAN_FRONTEND=non-interactive wget -O - "$IBR_DIST_URL/Release.key" | apt-key add -
RUN echo "deb $IBR_DIST_URL ./" > /etc/apt/sources.list.d/ibr.list

# update apt-get and install IBR packages
RUN apt-get update
RUN apt-get -y install ibrdtnd=$IBR_VERSION ibrdtn-tools=$IBR_VERSION

# copy configuration to container
ADD  support/IBR/*.conf /etc/ibrdtn/

# add net-setup
ADD scripts/net-setup.sh /tmp/net-setup.sh
RUN chmod +x /tmp/net-setup.sh

# start
CMD /tmp/net-setup.sh && dtnd $IBR_OPTS

