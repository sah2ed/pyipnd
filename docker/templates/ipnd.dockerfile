FROM ubuntu:14.04
MAINTAINER TCSDEVELOPER

# replace sh by bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# set ubuntu mirror
ENV UBUNTU_MIRROR {{ubuntu_mirror}}


# change sources.list to closest mirror and update package info
RUN sed -i "s/http:\/\/archive.ubuntu.com/http:\/\/$UBUNTU_MIRROR/g" /etc/apt/sources.list
RUN apt-get -y update
RUN apt-get upgrade -y

# install wget
RUN apt-get install wget -y

# set environment variables
{% for key, value in envs.items() %}
ENV {{key}} {{value}}
{% endfor %}

# install IPND dependencies
RUN apt-get update
RUN apt-get install -y python-pip python-dev build-essential 
RUN apt-get install -y libxml2-dev libxslt1-dev python-dev zlib1g-dev
RUN pip install netaddr bitarray netifaces pybloom docker-py beautifulsoup4 lxml

# copy IPND
ADD support/IPND/IPND /opt/IPND

# add net-setup
ADD scripts/net-setup.sh /tmp/net-setup.sh
RUN chmod +x /tmp/net-setup.sh

# start
WORKDIR /opt/IPND
CMD /tmp/net-setup.sh && python IPND.py -d $IPND_OPTS

